#ifndef DOCUMENT_H
#define DOCUMENT_H

#include <string>
#include <stdexcept>
using namespace std;

class document {
public:
    document();
    document(string usuari, int prioritat, string nom); 
    ~document();
    string getUsuari() const;
    int getPrioritat() const;
    string getNom() const;
    void setUsuari(string usuari);
    void setPrioritat(int prioritat);
    void setNom(string nom);
    friend std::ostream &operator<<(std::ostream &os, const document &doc);
    
    
    
private:
    string usuari;
    int prioritat;
    string nom;
    

};

#endif /* DOCUMENT_H */

