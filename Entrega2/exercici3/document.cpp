#include "document.h"
using namespace std;

/**
 * Constructor per defecte de document
 */
document::document()
{
    this->usuari = "";
    this->prioritat = 0;
    this->nom = "";
}

/**
 * Caonstructor parametritzat de document
 * @param usuari string amb el nom de l'usuari
 * @param prioritat  enter amb prioritat d'impressió del document [1 ó 2]
 * @param nom string amb el nom del fitxer
 */
document::document(string usuari, int prioritat, string nom)
{
    this->usuari = usuari;
    this->prioritat = prioritat;
    this->nom = nom;
}

/**
 * Destructor de fitxer
 */
document::~document() 
{
}

/**
 * Getter de l'usuari propietari del document
 * @return string amb el nom
 */
string document::getUsuari() const
{
    return this->usuari;
}

/**
 * Getter de la prioritat d'impressió
 * @return enter amb la prioritat [1 ó 2], 1 màxima prioritat
 */
int document::getPrioritat() const
{
    return this->prioritat;
}

/**
 * Getter del nom del document
 * @return string amb el nom del document
 */
string document::getNom() const
{
    return this->nom;
}

/**
 * Setter de l'usuari propietari del document
 * @param usuari 
 */
void document::setUsuari(string usuari)
{
    this->usuari = usuari;
}

/**
 * Setter de la prioritat del document
 * @param prioritat
 */
void document::setPrioritat(int prioritat)
{
    if(prioritat == 1 || prioritat == 2)
    {
        this->prioritat = prioritat;    
    }
    else
    {
        throw invalid_argument("La prioritat ha de ser 1 (alta) o bé 2 (baixa)");
    }
}

/**
 * Setter de l'usuari propietari
 * @param nom
 */
void document::setNom(string nom)
{
    this->nom = nom;
}

/**
 * Sobrecàrrega de l'operador << per poder imprimir objectes de tipus document directament
 * @param os
 * @param doc
 * @return 
 */
std::ostream &operator<<(std::ostream &os, const document &doc) 
{
    string out = doc.getUsuari() + " " + to_string(doc.getPrioritat()) + " " + doc.getNom();
    return os << out;
}

