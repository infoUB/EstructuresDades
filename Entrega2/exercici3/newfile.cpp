/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: agarcise78.alumnes
 *
 * Created on 19 / de març / 2018, 12:22
 */

#include <bits/stdc++.h>
#include "LinkedDeque.hpp"
#include "document.h"

using namespace std;

void mostrar_menu()
{
    cout << "1.- Llegir un fitxer amb les entrades de la cua d’impressió" << endl;
    cout << "2.- Eliminar una impressió pel davant" << endl;
    cout << "3.- Eliminar una impressió pel final" << endl;
    cout << "4.- Inserir n entrades d’impressió des de teclat (0 per finalizar)" << endl;
    cout << "5.- Imprimir cua d'impressió" << endl;
    cout << "6.- Sortir" << endl;
    cout << "====================================" << endl;
}

int main(int argc, char** argv) {
    
    LinkedDeque<document> *cua = new LinkedDeque<document>;
    int opcio;
    std::string in_file;
    do{
        cout << endl;
        mostrar_menu();
        cout << "Introdueixi l'opció a executar: ";
        cin >> opcio;
        while(opcio < 1 && opcio > 6)
        {
            cout << "Opció no vàlida." << endl;
            mostrar_menu();
            cin >> opcio;
        }
        
        switch(opcio)
        {            
            case 1:
            {
                cout << "Introdueixi el nom del fitxer a llegir:" << endl;
                cin >> in_file;
                std::ifstream file(in_file);

                while(!file.eof())
                {
                    string usuari;
                    int prioritat;
                    string nom;
                    
                    cin >> usuari >> prioritat >> nom;
                    document doc(usuari, prioritat, nom);
                    if(doc.getPrioritat() == 1)
                    {
                        cua->insertFront(doc);
                    }
                    else if(doc.getPrioritat() == 2)
                    {
                        cua->insertRear(doc);
                    }
                    else
                    {
                        cout << "Prioritat de document no vàlida." << endl;
                    }               
                }

            }
            
            case 2:
            {
                cout << "S'ha eliminat el document " << cua->getFront().getNom() << " de l'usuari " << cua->getFront().getUsuari() << "." << endl;
                cua->deleteFront();
                break;                
            }
            
            case 3:
            {
                cout << "S'ha eliminat el document " << cua->getRear().getNom() << " de l'usuari " << cua->getRear().getUsuari() << "." << endl;
                cua->deleteRear();
                break;    
            }
  
            case 4:
            {
                string entrada;
                cin >> entrada;
                while(entrada != "0")
                {
                    string usuari;
                    int prioritat;
                    string nom;
                    
                    cin >> usuari >> prioritat >> nom;
                    document doc(usuari, prioritat, nom);
                    if(doc.getPrioritat() == 1)
                    {
                        cua->insertFront(doc);
                    }
                    else if(doc.getPrioritat() == 2)
                    {
                        cua->insertRear(doc);
                    }      
                }
            }
            
            case 5:
            {
                cua->print();
                break;
            }
            
            case 6:
                delete cua;
                break;
                
             
        }
