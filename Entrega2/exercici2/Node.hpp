#ifndef NODE_HPP
#define NODE_HPP

template <class T>
class Node{
    public:
        Node();
        Node(T element);
        ~Node();
        const T& getElement() const;
        Node<T>* getNext();
        Node<T>* getPrevious();
        void setPrevious(Node<T>* prev);
        void setNext(Node<T>* next);
    
    private:
        T element;
        Node<T>* previous;
        Node<T>* next;
            
        
};

/**
 * Constructor per defecte de la classe Node
 */
template <class T>
Node<T>::Node()
{
    //this->element = -1;
    this->previous = nullptr;
    this->next = nullptr;   
}

/**
 * Constructor parametritzat de la classe Node
 * @param element Contingut del node
 */
template <class T>
Node<T>::Node(T element)
{
    this->element = element;
    this->previous = nullptr;
    this->next = nullptr;
}

/**
 * Destrunctor de la classe Node
 */
template <class T>
Node<T>::~Node()
{

}

/**
 * Getter del contingut de Node
 * @return contingut de node
 */
template <class T>
const T& Node<T>::getElement() const
{
    return this->element;
}

/**
 * Getter pel següent node
 * @return Punter de Node al segünet node
 */
template <class T>
Node<T>* Node<T>::getNext()
{
    return (this->next);
}

/**
 * Getter pel node anterior a l'actual
 * @return Punter de Node al node anterior
 */
template <class T>
Node<T>* Node<T>::getPrevious()
{
    return (this->previous);
}

/**
 * Setter del node següent
 * @param next punter de Node al node segünet
 */
template <class T>
void Node<T>::setNext(Node<T>* next)
{
    this->next = next;
}

/**
 * Setter del node anterior
 * @param prev punter de Node al node anterior
 */
template <class T>
void Node<T>::setPrevious(Node<T>* prev)
{
    this->previous = prev;
}

#endif /* NODE_HPP */
