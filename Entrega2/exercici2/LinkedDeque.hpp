#ifndef LINKEDDEQUE_H
#define LINKEDDEQUE_H

#include "Node.hpp"
#include <stdexcept>
#include <iostream>
template <class T>
class LinkedDeque
{
    public:
        LinkedDeque();
        LinkedDeque(const LinkedDeque<T>& deque);
        ~LinkedDeque(); 
        bool isEmpty() const;
        void insertFront(const T & element);
        void insertRear(const T & element);
        void deleteFront();
        void deleteRear();
        void print();
        void println();
        const T& getFront() const;  
        const T& getRear() const;
        Node<T>* getFrontNode() const;
        Node<T>* getRearNode() const;
        int size() const;
        
    private:
        Node<T>* _front;
        Node<T>* _rear;
        int num_elements;
};

/**
 * Constructor per defecte de la classe LinkedDeque
 */
template <class T>
LinkedDeque<T>::LinkedDeque()
{
    //Creem els nodes "centinella" i fem que _front i _rear els apuntin
    Node<T>* front = new Node<T>();
    this->_front = front;
    
    Node<T>* rear = new Node<T>();
    this->_rear = rear;
    
    //Movem els punters dels nodes
    (*_front).setNext(_rear);
    (*_rear).setPrevious(_front);
}

/**
 * Costructor còpia de la classe LinkedDeque
 * @param deque Cua que es vol copiar
 */
template <class T>
LinkedDeque<T>::LinkedDeque(const LinkedDeque<T>& deque)
{
    //Creem els nodes de front i rear
    Node<T>* front = new Node<T>();
    this->_front = front;
    
    Node<T>* rear = new Node<T>();
    this->_rear = rear;
    
    //Posem els punters dels nodes
    (*_front).setNext(_rear);
    (*_rear).setPrevious(_front);
    
    //Recorrem la deque que volem copiar i anem posant els seus elements a this
    Node<T>* it = deque.getFrontNode();
    for(int i = 0; i < deque.size(); i++)
    {
        this->insertRear(it->getElement());
        it = it->getNext();        
    }
}

/**
 * Destructor de LinkedDeque
 */
template <class T>
LinkedDeque<T>::~LinkedDeque()
{
    for(int i = 0; i < this->size(); i++)
    {
        this->deleteFront();
    }
    delete this->_front;
    delete this->_rear;
}

/**
 * Mètode per comprovar si la deque és buida
 * @return true si no hi ha cap element, false en cas contrari
 */
template <class T>
bool LinkedDeque<T>::isEmpty() const
{
    return this->num_elements == 0;
}

/**
 * Mètode per inserir un element a front creant un nou node
 * @param element contingut del node
 */
template <class T>
void LinkedDeque<T>::insertFront(const T& element)
{
    Node<T>* node = new Node<T>(element); //Creem un nou node
    Node<T>* tmp = _front->getNext(); //agafem referència del primer node reañ
    
    (*node).setNext((*_front).getNext()); //El següent del nou és el primer
    (*node).setPrevious(_front); //L'anterior del nou és el "centinella"
    
    (*_front).setNext(node); //El següent del next és el nou
    (*(*node).getNext()).setPrevious(node); //L'anterior del primer és el nou
    
    this->num_elements++; //Incrementem el contador d'elements
}

/**
 * Mètode per inserir un element al final creant un nou node
 * @param element contingut del nou node
 */
template <class T>
void LinkedDeque<T>::insertRear(const T& element)
{
    Node<T>* node = new Node<T>(element);
    Node<T>* tmp = _rear->getPrevious();
    
    (*node).setNext(_rear);
    (*node).setPrevious((*_rear).getPrevious());
    
    (*_rear).setPrevious(node);
    (*(*node).getPrevious()).setNext(node);
    
    this->num_elements++;
}

/**
 * Mètode per eliminar el primer node
 */
template <class T>
void LinkedDeque<T>::deleteFront()
{
    if(!isEmpty()) //Si la cua no és buida
    {
        Node<T>* tmp = this->getFrontNode(); //Punter al primer node real
        _front->setNext((*tmp).getNext()); //El primer serà el següent de l'actual
        (*tmp).getNext()->setPrevious(_front); //L'anterior del nou primer és front
        delete tmp; //Eliminem el node
    
        this->num_elements--; //Decrementem el nombre d'elements
    }
    else
    {
        throw std::underflow_error("La cua és buida");
    }
}

/**
 * Mètode per eliminar l'últim node
 */
template <class T>
void LinkedDeque<T>::deleteRear()
{
    if(!isEmpty())
    {
        Node<T>* tmp = this->getRearNode();
        _rear->setPrevious((*tmp).getPrevious());
        (*tmp).getPrevious()->setNext(_rear);
        delete tmp;

        this->num_elements--;
    }
    else
    {
        throw std::underflow_error("La cua és buida");
    }
}

/**
 * Mètode per imprimir el contingut de la LinkedDeque
 */
template <class T>
void LinkedDeque<T>::print()
{
    if(!isEmpty())
    {
        Node<T>* tmp = this->getFrontNode(); //Punter al primer node
        std::cout << "[";
        for(int i = 0; i < this->size()-1; i++) //Recorrem la deque
        {
            std::cout << tmp->getElement() << ", "; 
            tmp = tmp->getNext(); //Agafem el següent node
        }
        std::cout << this->getRearNode()->getElement() << "]" << std::endl;
    }
    else
    {
        std::cout << "[]" << std::endl;
    }
    
}

/**
 * Mètode alternatiu per imprimir la LinkedDeque que imprimeix un element a cada línia.
 */
template <class T>
void LinkedDeque<T>::println()
{   
    if(!isEmpty())
    {
        std::cout << "Elements de la cua: " << std::endl;
        Node<T>* tmp = this->getFrontNode();
        for(int i = 0; i < this->size(); i++)
        {
            std::cout << "- " <<tmp->getElement() << std::endl;
            tmp = tmp->getNext();
        }
    }
    else
    {
        std::cout << "No hi ha elements a mostrar, la cua és buida." << std::endl;
    }
}

/**
 * Mètode per obtenir el contingut del primer node
 * @return contingut del primer node 
 */
template <class T>
const T& LinkedDeque<T>::getFront() const
{
    return this->getFrontNode()->getElement();
}

/**
 * Mètode per obtenir el contingut de l'últim node
 * @return contingut de l'últim node
 */
template <class T>
const T& LinkedDeque<T>::getRear() const
{
    return this->getRearNode()->getElement();
}

/**
 * Mètode per obtenir un punter al primer node de la deque
 * @return punter de node al primer node
 */
template <class T>
Node<T>* LinkedDeque<T>::getFrontNode() const
{
    return (*_front).getNext();
}

/**
 * Mètode per obtenir un punter a l'últim node de la deque
 * @return 
 */
template <class T>
Node<T>* LinkedDeque<T>::getRearNode() const
{
    return (*_rear).getPrevious();
}

/**
 * Mètode per obtenir el nombre d'elements de la deque
 * @return Enter corresponent al nombre de nodes de la deque (sense "centinelles")
 */
template <class T>
int LinkedDeque<T>::size() const
{
    return num_elements;
}


#endif /* LINKEDDEQUE_H */
