#include <bits/stdc++.h>


#include "LinkedDeque.hpp"

using namespace std;

void mostrar_menu()
{
    cout << "1.- Inserir element pel davant" << endl;
    cout << "2.- Inserir element pel final" << endl;
    cout << "3.- Eliminar element del davant" << endl;
    cout << "4.- Elinimar element del final" << endl;
    cout << "5.- Imprimir contingut" << endl;
    cout << "6.- Sortir" << endl;
    cout << "====================================" << endl;
}

int main(int argc, char** argv) {
    
    LinkedDeque<int> *cua = new LinkedDeque<int>;
    int opcio;
    do{
        cout << endl;
        mostrar_menu();
        cout << "Introdueixi l'opció a executar: ";
        cin >> opcio;
        while(opcio < 1 && opcio > 6)
        {
            cout << "Opció no vàlida." << endl;
            mostrar_menu();
            cin >> opcio;
        }
        
        switch(opcio)
        {            
            case 1:
            {
                int element;
                cout << "Introdueixi el nombre que vol afegir:";
                cin >> element;
                try
                {
                    cua->insertFront(element);
                    cout << "Element " << element << " afegit" << endl;

                }
                catch(exception &e)
                {
                    cout << e.what() << endl;
                }
                break;
            }
            
            case 2:
            {
                int element;
                cout << "Introdueixi el nombre que vol afegir:";
                cin >> element;
                try
                {
                    cua->insertRear(element);
                    cout << "Element " << element << " afegit" << endl;
                }
                catch(exception &e)
                {
                    cout << e.what() << endl;
                }
                break;                
            }
            
            case 3:
            {
                try
                {
                    int element = cua->getFront();
                    cua->deleteFront();
                    cout << "Element " << element << " eliminat" << endl;
                }
                catch(exception &e)
                {
                    cout << e.what() << endl;
                }
                break;   
            }
  
            case 4:
            {
                try
                {
                    int element = cua->getRear();
                    cua->deleteRear();
                    cout << "Element " << element << " eliminat" << endl;
                    
                }
                catch(exception &e)
                {
                    cout << e.what() << endl;
                }
                break;   
            }
            
            case 5:
            {
                cua->print();
                break;
            }
            
            case 6:
                delete cua;
                break;
                
             
        }

        
        
        
    } while(opcio != 6);
}
/*
///////////////
// PREGUNTES //
///////////////

  1.- Sí, hem utilitzat templates. Ho hem fet així perquè, d'aquesta manera,
 *      la LinkedDeque no està limitada a contenir variables d'un tipus 
 *      concret, sinó que podem fer una LinkedDeque de qualsevol tipus.

  2.- 
    LinkedDeque() -> O(1), ja que només creem 2 nodes i movem punters (tot de cost O(1)).
    LinkedDeque(const LinkedDeque<T>& deque) -> O(n), ja que hem de recòrrer tota la deque
 *      paràmetre per fer la còpia de tots els nodes.
    ~LinkedDeque() -> O(n), ja que hem de fer de removeFront (O(1)) per a cada node que hi hagi.
    bool isEmpty() const -> O(1), només cal consultar una variable
    void insertFront(const T & element) -> O(1), només hem de crear un node i moure punters (tot de O(1)).
    void insertRear(const T & element) -> O(1), només hem de crear un node i moure punters (tot de O(1)).
    void deleteFront() -> O(1), només hem d'eliminar un node i moure punters (tot de O(1)).
    void deleteRear()-> O(1), només hem d'eliminar un node i moure punters (tot de O(1)).
    void print() -> O(n), ja que hem de recòrrer tota la deque per imprimir tots els components
    const T& getFront() const -> O(1), ja que només s'ha de consultar el continut d'un node
    const T& getRear() const -> O(1), ja que només s'ha de consultar el continut d'un node
    Node<T>* getFrontNode() const -> O(1), ja que només s'ha de retornar un punter
    Node<T>* getRearNode() const -> O(1), ja que només s'ha de retornar un punter
    int size() const -> O(1), ja que només s'ha de retornar una variable
 
3.- No, només complicaria el programa i faia que els costos computacionals augmentessin.
 *      Al tenir enllaços dobles podem accedir al node anterior directament, amb enllaços
 *      simples hauríem de recòrrer una part de la deque cada vegada per trobar l'anterior
*/
