#include "ArrayDeque.h"
#include <exception>
#include <iostream>

using namespace std;

/**
 * Constructor de la classe ArrayDeque
 * @param maxSize Mida màxima de l'ArrayDeque
 */
ArrayDeque::ArrayDeque(int maxSize){
    if (maxSize <= 0){
        throw invalid_argument("La mida màxima ha de ser estríctament positiva.");
    }
    this->MAX_SIZE = maxSize;
    this->count = 0;
    this->data = new vector<int>(MAX_SIZE);
    this->front = (MAX_SIZE/2); //Front és la primera posició ocupada
    this->rear = MAX_SIZE/2;  //Rear és la primera posició buida
    //Posem les variables al mig, però podrien ser a qualsevol altra posició (perquè el vector és circular)
    //Inicialment són les mateixes, però a l'afegir el primer element ja es separaran
   
}

/**
 * Destructor de la classe ArrayDeque
 */
ArrayDeque::~ArrayDeque() {
    delete data;
    cout << "Destructor cua" << endl;
}

/**
 * Mètode per comprovar si la cua és buida
 * @return <code> true </code> si està buida, <code> false </code> en cas contrari
 */
bool ArrayDeque::isEmpty() const{
    return (count <= 0);
}

/**
 * Mètode per comprovar si la cua és plena
 * @return <code> true </code> si està plena, <code> false </code> en cas contrari
 */
bool ArrayDeque::isFull() const{
    return (count == MAX_SIZE); //Si la següent posició de rear és front, la cua és plena
}

/**
 * Mètode per afegir un element a l'inici de la cua
 * @param element Enter que es vol afegir al principi de la cua
 */
void ArrayDeque::insertFront(int element){
    if(isFull()) //Si és ple
    {
        throw overflow_error("Cua plena."); //Llencem excepció
    }
    else
    {
        (*data)[mod(front-1, MAX_SIZE)] = element;
        front = mod(front-1, MAX_SIZE);
        count++;
    }
}

/**
 * Mètode per afegir un element al final de la cua
 * @param element Enter que es vol afegir al final de la cua
 */
void ArrayDeque::insertRear(int element){
    if(isFull())
    {
        throw overflow_error("Cua plena.");
    }
    else
    {
        (*data)[rear] = element;
        rear = mod(rear+1, MAX_SIZE);
        count++;
    }
}

/**
 * Mètode per eliminar el primer element de la cua
 */
void ArrayDeque::deleteFront(){
    if(isEmpty())
    {
        throw underflow_error("Cua buida.");
    }
    else
    {
        (*data)[front] = 0;
        front = mod(front+1, MAX_SIZE);
        count--;
    }
} 

/**
 * Mètode per eleiminar el darrer element de la cua
 */
void ArrayDeque::deleteRear(){
    if(isEmpty())
    {
        throw underflow_error("Cua buida.");
    }
    else
    {
        (*data)[rear-1] = 0;
        rear = mod(rear-1, MAX_SIZE);
        count--;
    }
}

/**
 * Mètode per impimir la cua per pantalla (cout)
 */
void ArrayDeque::print() const{
    if(isEmpty()) //Si és buida no volem mostrar ni rear
    {
        cout << "[]" << endl;
    }
    else
    {
        cout << "[";
        for(int i = 0; i < count-1; i++) //Iterem des de 0 fins al nombre d'elements excepte l'últim
        {
            cout << (*data)[mod(front + i, MAX_SIZE)] << ", "; //Imprimim fornt + i de manera que ens queden en l'ordre correcte
        }
        cout << (*data)[mod(rear-1, MAX_SIZE)] << "]" << endl; //Imprimim rear al final perquè no porta "," darrere seu.
    }
}

/**
 * Mètode per obenir el primer element de la cua
 * @return El primer enter de la cua
 */
int ArrayDeque::getFront() const{
    if(this->isEmpty())
    {
        throw underflow_error("Cua buida");
    }
    else
    {
        return (*data)[mod(front, MAX_SIZE)]; 
    }
}

/**
 * Mètode per obtenir el darrer element de la cua
 * @return 
 */
int ArrayDeque::getRear() const{
    if(this->isEmpty())
    {
        throw underflow_error("Cua buida");
    }
    else
    {
        return (*data)[mod(rear-1, MAX_SIZE)]; 
    }
}

/**
 * Mètode de classe per solucionar congruències del tipus a = x (mod b)
 * @param a
 * @param b
 * @return x
 */
int ArrayDeque::mod(int a, int b) //Funció mòdul en lloc de % (residu), permetent funcionar amb negatius.
{ 
    return (a%b+b)%b;
}