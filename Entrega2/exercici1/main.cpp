/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: mherralo19.alumnes
 *
 * Created on 5 / de març / 2018, 12:57
 */

#include <iostream>
#include "ArrayDeque.h"

using namespace std;

void mostrar_menu()
{
    cout << "0.- Crear un ArrayDeque" << endl;
    cout << "1.- Inserir element pel davant" << endl;
    cout << "2.- Inserir element pel final" << endl;
    cout << "3.- Eliminar element del davant" << endl;
    cout << "4.- Elinimar element del final" << endl;
    cout << "5.- Imprimir contingut" << endl;
    cout << "6.- Sortir" << endl;
    cout << "====================================" << endl;
}

int main(int argc, char** argv) {
    
    ArrayDeque *cua;
    int opcio;
    do{
        cout << endl;
        mostrar_menu();
        cout << "Introdueixi l'opció a executar: ";
        cin >> opcio;
        while(opcio < 0 && opcio > 6)
        {
            cout << "Opció no vàlida." << endl;
            mostrar_menu();
            cin >> opcio;
        }
        
        switch(opcio)
        {
            case 0:
            {
                int mida;
                cout << "De quina mida voleu que sigui la cua?" << endl;
                cin >> mida;
                
                try
                {
                    cua = new ArrayDeque(mida);
                }
                catch(exception &e)
                {
                    cout << e.what() << endl;
                }
                break;
            }
            
            case 1:
            {
                int element;
                cout << "Introdueixi el nombre que vol afegir:";
                cin >> element;
                try
                {
                    cua->insertFront(element);
                    cout << "Element " << element << " afegit" << endl;

                }
                catch(exception &e)
                {
                    cout << e.what() << endl;
                }
                break;
            }
            
            case 2:
            {
                int element;
                cout << "Introdueixi el nombre que vol afegir:";
                cin >> element;
                try
                {
                    cua->insertRear(element);
                    cout << "Element " << element << " afegit" << endl;
                }
                catch(exception &e)
                {
                    cout << e.what() << endl;
                }
                break;                
            }
            
            case 3:
            {
                try
                {
                    int element = cua->getFront();
                    cout << "Element " << element << " eliminat" << endl;
                    cua->deleteFront();

                }
                catch(exception &e)
                {
                    cout << e.what() << endl;
                }
                break;   
            }
  
            case 4:
            {
                try
                {
                    int element = cua->getRear();
                    cout << "Element " << element << " eliminat" << endl;
                    cua->deleteRear();
                }
                catch(exception &e)
                {
                    cout << e.what() << endl;
                }
                break;   
            }
            
            case 5:
            {
                cua->print();
                break;
            }
            
            case 6:
                delete cua;
                break;
                
             
        }

        
        
        
    } while(opcio != 6);
}

