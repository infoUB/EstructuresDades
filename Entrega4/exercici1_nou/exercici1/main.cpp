#include <iostream>

#include "MinHeap.hpp"

using namespace std;

void mostrar_menu()
{
    cout << "0.- Executar test de prova" << endl;
    cout << "1.- Inserir element" << endl;
    cout << "2.- Imprimir" << endl;
    cout << "3.- Imprimir ordenat" << endl;
    cout << "4.- Mostrar element mínim" << endl;
    cout << "5.- Cercar un valor" << endl;
    cout << "6.- Mostrar alçada" << endl;
    cout << "7.- Eliminar i sortir" << endl;
}

void array_to_heap(int arr[], int size, MinHeap<int> &heap)
{
    for(int i = 0; i < size; i++)
    {
        heap.insert(arr[i], arr[i]);
        cout << "Inserint element " << arr[i] << " a l'heap." << endl;
    }
}

void test_1()
{
    int arr[] = {2, 0, 8, 45, 76, 5, 3, 40};
    MinHeap<int> *heap = new MinHeap<int>();
    cout << "Heap creat." << endl << endl;
    array_to_heap(arr,sizeof(arr)/sizeof(int), *heap);

    cout << "Imprimint heap segons l'ordre de vector: " << endl; 
    heap->printHeap();    

    cout << "Imprimint heap ordenat: " << endl;
    heap->printInOrder();
}   

/*
 * 
 */
int main(int argc, char** argv)
{
    MinHeap<int> *heap = new MinHeap<int>();
    int opcio;
    do{
        cout << endl;
        mostrar_menu();
        cout << "Introdueixi l'opció a executar: ";
        cin >> opcio;
        while(opcio < 1 && opcio > 7)
        {
            cout << "Opció no vàlida." << endl;
            mostrar_menu();
            cin >> opcio;
        }
        
        switch(opcio)
        {
            
            case 0:
            {
                test_1();
                break;
            }
            case 1:
            {
                int value;
                cout << "Introduiexi el nombre que vol afegir:" << endl;
                cin >> value;
                try{
                    heap->insert(value, value);
                    cout << "Nombre " << value << " afegit." << endl;
                }
                catch(exception &e)
                {
                    cout << e.what() << endl;
                }
                break;
            }
            
            case 2:
            {
                heap->printHeap();
                break;
            }
            
            case 3:
            {
                heap->printInOrder();
                break;
            }
            
            case 4:
            {
                try
                {
                    cout << "L'element mínim és: " << heap->min() << endl;
                }
                catch(exception &e)
                {
                    cout << e.what() << endl;
                }
                break;
            }
            
            case 5:
            {
                cout << "Introdueixi l'element a cercar:";
                int elem;
                cin >> elem;
                try
                {
                    cout << "Element trobat: " << heap->search(elem) << endl;
                }
                catch(exception &e)
                {
                    cout << e.what() << endl;
                }
                break;
            }
            
            case 6:
            {
                cout << "Alçada de l'heap: " << heap->getHeight() << endl;
                break;
            }
            
            case 7:
            {
                delete heap;
                return 0;
            }
        }
    } while(opcio != 7);
}

