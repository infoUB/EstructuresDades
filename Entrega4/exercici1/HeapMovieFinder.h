/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   HeapMovieFinder.h
 * Author: aniol
 *
 * Created on 24 / de maig / 2018, 00:33
 */

#ifndef HEAPMOVIEFINDER_H
#define HEAPMOVIEFINDER_H

#include "Movie.h"
#include "Node.hpp"
#include "MinHeap.hpp"

class HeapMovieFinder {
public:
    HeapMovieFinder();
    HeapMovieFinder(const HeapMovieFinder& orig);
    virtual ~HeapMovieFinder();
    void insertMovie(int movieID, string title, float rating);
    void appendMovies(string filename);
    string showMovie(int id);
    void findMovie(int id);
    void print();
    int getHeight();
    
    
private:
    MinHeap<Movie> heap;

};

#endif /* HEAPMOVIEFINDER_H */

