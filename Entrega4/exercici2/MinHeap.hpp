#ifndef MINHEAP_HPP
#define MINHEAP_HPP

#include <iostream>
#include <vector>
#include "Node.hpp"
#include <cmath>
using namespace std;

template <class Type>
class MinHeap
{
    public:
        /*Constructors i destructor*/
        MinHeap();
        MinHeap(MinHeap& orig);
        virtual ~MinHeap();

        /*Consultors*/
        int size();
        bool empty();
        const int min();
        const Type minValues();
        void printHeap();
        const Type search(int id);
        int getMinChild(int i);
        void printInOrder();
        int getHeight();

        /*Modificadors*/
        void insert(int key, Type val);
        Type removeMin();
        
    private:
        vector<Node<Type> > heap;
};

// -----------------------------------------
//    Constructors i destructors
// -----------------------------------------

/**
 * Constructor de la classe MinHeap
 */
template <class Type>
MinHeap<Type>::MinHeap()
{
}

/**
 * Cosntructor còpia de la classe MinHeap
 * @param orig MinHeap original a copiar
 */
template <class Type>
MinHeap<Type>::MinHeap(MinHeap& orig)
{
    this->heap = orig.heap;
}

/**
 * Destructor de la classe MinHeap
 */
template <class Type>
MinHeap<Type>::~MinHeap()
{
}

// -----------------------------------------
//    Funcions consultores
// -----------------------------------------

/**
 * Getter de size de MinHeap
 * @return enter amb la mida del vector heap
 */
template <class Type>
int MinHeap<Type>::size()
{
    return this->heap.size();
}

/**
 * Getter per a saber si el heap és buit
 * @return boleà segons si la mida és zero o no
 */
template <class Type>
bool MinHeap<Type>::empty()
{
    return this->size() == 0;
}

/**
 * Getter de la clau de l'element mínim del heap.
 * @return un enter amb la clau de l'element 
 * situat en la primera posició del vector
 */
template <class Type>
const int MinHeap<Type>::min()
{
    if(!this->empty())
    {
        return this->heap[0].getKey();
    }
    throw logic_error("El heap és buit.");
}

/**
 * Getter del valor de l'element mínim del heap
 * @return un objecte Type amb el valor de l'element
 * situat en la primera posició del vector
 */
template <class Type>
const Type MinHeap<Type>::minValues()
{
    if(!this->empty)
    {
        return this->heap[0].getValue();
    }
    throw logic_error("El heap és buit.");
}

/**
 * Imprimeix els valors del heap en l'ordre
 * del vector.
 */
template <class Type>
void MinHeap<Type>::printHeap()
{
    for (int i=0; i<heap.size(); i++)
    {
        cout << heap[i].getValue() << endl;
    }
}

/**
 * Busca el node amb clau igual a una donada 
 * i retorna el seu valor.
 * @param id
 * @return 
 */
template <class Type>
const Type MinHeap<Type>::search(int id)
{
    for (int i=0; i<heap.size(); i++)
    {
        if(heap[i].getKey()==id)
        {
            return heap[i].getValue();
        }
    }
    throw invalid_argument("La clau no coincideix amb cap valor del heap.");
}

/**
 * Retorna la posició del fill amb clau 
 * mínima del node en la posició donada.
 * @param i
 * @return 
 */
template <class Type>
int MinHeap<Type>::getMinChild(int i)
{
    if(heap[i*2+1].getKey() < heap[i*2+2].getKey())
        return i*2+1;
    return i*2+2;
}

/**
 * Retorna l'alçada del heap
 * @return 
 */
template <class Type>
int MinHeap<Type>::getHeight()
{
    return floor(log(heap.size())/log(2)) + 1;
}

/**
 * Imprimeix el MinHeap en ordre creixent.
 */
template <class Type>
void MinHeap<Type>::printInOrder()
{
    MinHeap<Type> copia(*this);
    while (!copia.empty())
    {
        cout << copia.removeMin() <<endl;
    }
}

/**
 * Insereix un nou node i reendreça el heap
 * per a conservar les propietats.
 * @param key
 * @param val
 */
template <class Type>
void MinHeap<Type>::insert(int key, Type val)
{
    Node<Type> nou(key, val); //Creem el nou node
    heap.push_back(nou); //L'inserim al final del vector
    int p = heap.size()-1;
    while (p!=0 && heap[p].getKey() < heap[floor((p-1)/2)].getKey()) //Mentre no arribi a dalt i el fill sigui més petit que el pare
    {
        swap(heap[p], heap[floor((p-1)/2)]); //Canviem l'actual pel pare
        p = floor((p-1)/2); //Repetim el procés pel pare
    }
}

/**
 * Elimina l'element mínim del heap i el 
 * reendreça.
 * @return 
 */
template <class Type>
Type MinHeap<Type>::removeMin()
{
    Type min = heap[0].getValue(); //Guardem el valor del mínim
    if(heap.size() != 1) //Si hi ha més d'un node
    {
        swap(heap[0], heap[heap.size()-1]);  //Canviem el primer per l'últim
        heap.pop_back(); //Eliminem l'últim
        int p = 0;
        while(p*2+1 <= heap.size()-1) //Mentre tingui fill esquerre
        {
            if (p*2+2 <= heap.size()-1 && (heap[p].getKey() > heap[p*2+1].getKey() || heap[p].getKey() > heap[p*2+2].getKey())) //Si també té fill dret i el pare és major que algun dels fills
            {
                int minChild = getMinChild(p); //Busquem el mínim
                swap(heap[p], heap[minChild]) ; //Els intercanviem
                p = minChild; //Actalitzem node actual
            }
            else if (heap[p*2+1].getKey() < heap[p].getKey()) //Si només té fill esquerre i aquest és menor que el pare
            {
                swap(heap[p], heap[p*2+1]); //Canviem
                p = p*2+1; //Actualitzem
            }
            else //Si no hem de canviar res, podem assegurar que es compleixen les propietats de l'arbre, per tant, em acabat.
                break;
        }
        return min; //Retornem el valor que havíem guardat
    }
    else //Si és l'últim node
    {
        heap.pop_back(); //L'eliminem
        return min; //Retornem el mínim
    }
}


#endif /* MINHEAP_HPP */

