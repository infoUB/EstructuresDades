#include "HeapMovieFinder.h"

HeapMovieFinder::HeapMovieFinder() 
{
}


HeapMovieFinder::~HeapMovieFinder() 
{
}

void HeapMovieFinder::insertMovie(int id, string title, float rating)
{
    Movie mov(id, title, rating);
    this->heap.insert(id, mov);
    
}

void HeapMovieFinder::appendMovies(string filename)
{
    std::ifstream file(filename);
    string movie;
    while(getline(file, movie)) //Anem agafant línies
    {
        string id_string, title, rating_string; 
        int id;
        float rating;        
        int end = movie.find("::"); //Busquem el primer separador
        id_string = movie.substr(0, end); //Guardem la ID
        movie.erase(0, end + 2); //Esborrem la part que ja no volem llegir
        end = movie.find("::"); //Busquem el segon separador
        title = movie.substr(0, end); //Guardem el títol
        movie.erase(0, end+2); //Esborrem el títol i el separador
        rating_string = movie; //El que queda és la puntuació
        
        istringstream ss(id_string); //Passem la ID a enter
        ss >> id;
        istringstream ss2(rating_string); //Passem la puntuació a float
        ss2 >> rating;
        
        try
        {
            insertMovie(id, title, rating); //Inserim pel·lícula
        }
        catch(exception &e)
        {
            cout << e.what() << endl;
        }
    }
}

string HeapMovieFinder::showMovie(int id)
{
    return this->heap.search(id).to_string();
}

void HeapMovieFinder::findMovie(int id)
{
    cout << this->heap.search(id) << endl;
}

void HeapMovieFinder::print()
{
    MinHeap<Movie> copia(this->heap);
    int i = 0;
    while (!copia.empty())
    {
        if(i == 40)
        {
            
            cout << "Voleu seguir imprimint? [S/n]" << endl;
            char entrada;
            cin >> entrada;
            if(entrada == 'n') //Si no volem continuar imprimint
            {
                return;
            }
            else
            {
                i = 0;  //Si volem continuar imprimint (opció per defecte) posem el contador a 0
            }
        }
        cout << copia.removeMin() <<endl;
        i++;
    }
}

int HeapMovieFinder::getHeight()
{
    this->heap.getHeight() - 1;
}
