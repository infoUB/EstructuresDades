#include "Movie.h"

/**
 * Constructor per defecte de la classe Movi
 */
Movie::Movie()
{
    this->id = 0;
    this->title = "";
    this->score = 0;
}

/**
 * Constructor de la classe Movie
 * @param id ID de la pel·lícula
 * @param title títol
 * @param score Puntuació
 */
Movie::Movie(int id, string title, float score)
{
    this->id = id;
    this->title = title;
    this->score = score;
}

/**
 * Constructor còpia de la classe Movie
 * @param orig
 */
Movie::Movie(const Movie& orig)
{
    this->id = orig.get_id();
    this->title = orig.get_title();
    this->score = orig.get_score();
}

/**
 * Destructor de Movie
 */
Movie::~Movie() 
{
}

/**
 * Setter de la ID
 * @param id enter amb la id
 */
void Movie::set_id(int id)
{
    this->id = id;
}

/**
 * Seter del score
 * @param score float amb la nova puntuació
 */
void Movie::set_score(float score)
{
    this->score = score;
}

/**
 * Setter del títol
 * @param title string amb el títol
 */
void Movie::set_title(string title)
{
    this->title = title;
}

/**
 * Getter de la ID
 * @return enter amb la ID
 */
int Movie::get_id() const
{
    return this->id;
}

/**
 * Getter del títol
 * @return string amb el títol
 */
string Movie::get_title() const
{
    return this->title;
}

/**
 * Getter de la puntuació
 * @return float amb la puntuació
 */
float Movie::get_score() const
{
    return this->score;
}


/**
 * Mètode per obtenir tota la informació de Movie
 * @return string amb tota la informació de miovie
 */
string Movie::to_string() const
{
    return (std::to_string(id) + "::" + title + "::" + std::to_string(score));
}

/**
 * Sobreescriptura de l'operador << per poder fer cout directament
 * @param os
 * @param movie
 * @return 
 */
ostream& operator<<(ostream& os, const Movie& movie)
{
    os << movie.to_string();
    return os;
}

/**
 * Sobrecàrrega de l'operador d'assignació (=)
 * @param movie objecte Movie que es vol assignar
 * @return objecte movie amb els atributs modificats
 */
Movie& Movie::operator=(const Movie& movie)
{
    id = movie.get_id();
    title = movie.get_title();
    score = movie.get_score();
}