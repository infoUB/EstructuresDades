
#include <bits/stdc++.h>

#include "BSTMovieFind.h"

using namespace std;

void mostrar_menu()
{
    cout << "1.- Crear i omplir un arbre" << endl;
    cout << "2.- Mostrar arbre" << endl;
    cout << "3.- Cercar películes de cercaPelicules.txt a l'arbre" << endl;
    cout << "4.- Cercar la pel·lícula amb el títol més llarg" << endl;
    cout << "5.- Cercar la pel·lícula amb la puntiació més alta" << endl;       
    cout << "6.- Mostrar alçada de l'arbre" << endl;
    cout << "7.- Sortir" << endl;
}

int main(int argc, char** argv) 
{

    
    BSTMovieFind* tree = nullptr;
    cout << "TEST DE BSTMovieFind" << endl;
    int opcio;
    do{
        cout << endl;
        mostrar_menu();
        cout << "Introdueixi l'opció a executar: ";
        cin >> opcio;
        while((opcio < 1 || opcio > 7) || (opcio != 1 && opcio != 7 && tree == nullptr))
        {
            cout << "Opció no vàlida, ";
            if(opcio < 1 || opcio > 7)
            {
                cout << "índex fora de rang." << endl;
            }
            else
            {
                cout << "encara no heu inicialitzat l'arbre." << endl;
            }
            cout << endl;
                    
            mostrar_menu();
            cin >> opcio;
        }

        switch(opcio)
        {
            case 1:
            {
                delete tree;
                tree = new BSTMovieFind();
                cout << "Quin fitxer voleu utilitzar per a les proves? [g/p]" << endl;
                string entrada;
                cin >> entrada;
                while(entrada != "g" && entrada != "p" && entrada != "G" && entrada != "P")
                {
                    cout << "Entrada no vàlida. Siusplau, introduïu 'g' o 'p'" << endl;
                    cin >> entrada;
                }

                cout << "Inicialitzant l'arbre amb el fitxer escollit..." << endl;
                double temps_inici;
                if(entrada == "g" || entrada == "G")
                {
                    clock_t start = clock();
                    tree->appendMovies("movie_rating.txt");
                    clock_t end = clock();
                    temps_inici = double(end - start);         
                }
                else
                {
                    clock_t start = clock();
                    tree->appendMovies("movie_rating_small.txt");
                    clock_t end = clock();
                    temps_inici = double(end - start);
                }

                cout << "Arbre incialitzat. Temps transcorregut: " << temps_inici << " ticks (uns " << temps_inici/CLOCKS_PER_SEC << " segons)." << endl << endl;
                break;
            }

            case 2:
            {        
                cout << "Mostrant películes en ordre creixent d'ID:" << endl;
                try
                {
                    tree->printSorted();
                }
                catch(exception &e)
                {
                    cout << e.what()  << endl;
                }
                break;
            }

            case 3:
            {
                cout << "Cercant películes de cercaPelicules.txt a l'arbre:" << endl;
                ifstream file("cercaPelicules.txt");
                int id, cont = 0;
                clock_t start = clock();
                while(file >> id)
                {
                    try
                    {
                        tree->findMovie(id);
                        cont++;
                    }
                    catch(exception &e)
                    {
                    //Aquí no hi podem fer res... Si find movies tornés una referència podríem comprovar si és null...    
                    }
                }
                clock_t end = clock();
                file.close();
                double temps_cerca = double(end - start);
                cout << "S'ha trobat un total de " << cont << " coincidències en un total de " << temps_cerca << " ticks (uns " << temps_cerca/CLOCKS_PER_SEC << " segons)." << endl;
                break;
            }
            
            case 4:
            {
                cout << "Cercant la pel·lícula amb el títol més llarg..." << endl;
                Movie movie;
                movie = tree->longestTitle();
                cout << "La pelicula amb el títol més llarg és: " << movie.get_title() << " amb " << movie.get_title().length() << " caràcters." << endl;
                break;
            }
            
            case 5:
            {
                cout << "Buscant les puntuacions més altes i baixes..." << endl;
                vector<vector<Movie> > vec = tree->bestWorst();
                cout << "Les películes amb puntuació més alta són:"  << endl;
                for(int i = 0; i < vec[0].size(); i++)
                {
                    cout << "   " << vec[0][i].get_title() << " amb puntuació de " << vec[0][i].get_score() << endl;
                }
                
                cout << "Les películes amb puntuació més baixa són:"  << endl;
                for(int i = 0; i < vec[1].size(); i++)
                {
                    cout << "   " << vec[1][i].get_title() << " amb puntuació de " << vec[1][i].get_score() << endl;
                }
                break;
            }

            case 6:
            {
                cout << "L'arbre té una alçada de " << tree->getHeight() << " nivells." << endl;
                break;
            }

            case 7:
            {
                delete tree;
                break;
            }
        }
    } while(opcio != 7);
}
