template <class Type>
class NodeTree
{
    public:
        /*Constructors i destructor*/
        NodeTree(const Type& data); 
        NodeTree(const NodeTree& orig); 
        virtual ~NodeTree();
        
        /*Consultors*/
        NodeTree<Type>* getRight() const;
        NodeTree<Type>* getLeft() const;
        NodeTree<Type>* getParent() const;
        bool hasRight() const;
        bool hasLeft() const;
        bool isRoot() const;
        bool isExternal() const;
        const Type& getElement() const;
        int getHeight() const;
        
        /*Modificadors*/
        void setHeight(int h);
        void setData(const Type& data);
        void setRight(NodeTree<Type>* newRight);
        void setLeft(NodeTree<Type>* newLeft);
        void setParent(NodeTree<Type>* newParent);
        
    private:
        NodeTree<Type>* pParent;
        NodeTree<Type>* pLeft;
        NodeTree<Type>* pRight;
        Type data;
        int height;
};


// -----------------------------------------
//    Constructors i destructors
// -----------------------------------------


/**
 * Constructor de la classe NodeTree
 * @param data l'element que volem que contingui el node
 */
template <class Type> 
NodeTree <Type>:: NodeTree(const Type& data)
{
    this->pParent = nullptr;
    this->pLeft = nullptr;
    this->pRight = nullptr;
    this->data = data;
    this->height = 0;
}

/**
 * Cosntructor còpia de la classe NodeTree
 * @param orig node original a copiar
 */
template <class Type> 
NodeTree <Type>:: NodeTree(const NodeTree& orig)
{
    this->pParent = orig.getParent();
    this->pLeft = orig.getLeft();
    this->pRight = orig.getRight();
    this->data = orig.getElement();
    this->height = orig.getHeight();
}

/**
 * Destructor de la classe NodeTree
 */
template <class Type> 
NodeTree <Type>:: ~NodeTree()
{
    delete this->pLeft;
    delete this->pRight;
    this->pLeft = nullptr;
    this->pRight = nullptr;
    this->pParent = nullptr;
}


// -----------------------------------------
//    Funcions consultores
// -----------------------------------------


/**
 * Getter del node fill esquerre
 * @return punter de NodeTree al fill esquerre
 */
template <class Type> 
NodeTree<Type>* NodeTree <Type>:: getLeft() const
{
    return this->pLeft;
}

/**
 * Getter del node fill dret
 * @return punter de NodeTree al fill dret
 */
template <class Type> 
NodeTree<Type>* NodeTree <Type>:: getRight() const
{
    return this->pRight;
}

/**
 * Getter del node pare
 * @return punter de NodeTree al pare
 */
template <class Type> 
NodeTree<Type>* NodeTree <Type>:: getParent() const
{
    return this->pParent;
}

/**
 * Mètode per comprovar si el node té fill esquerre
 * @return booleà segons si té fill esquerre o no
 */
template <class Type> 
bool NodeTree <Type>:: hasLeft() const
{
    return (pLeft!=nullptr);
}

/**
 * Mètode per comprovar si el node té fill dret
 * @return booleà segons si té fill dret o no
 */
template <class Type> 
bool NodeTree <Type>:: hasRight() const
{
    return (pRight!=nullptr);
}

/**
 * Mètode per comprovar si el node és l'arrel d'un arbre
 * @return booleà segons si és arrel o no
 */
template <class Type>
bool NodeTree <Type>:: isRoot() const
{
    return (pParent == nullptr);
}

/**
 * Mètode per comprovar si el node és una fulla
 * @return booleà segons si és fulla o no
 */
template <class Type>
bool NodeTree <Type>:: isExternal() const
{
    return (!this->hasRight() and !this->hasLeft());
}

/**
 * Getter de l'element que guarda el node
 * @return element del node
 */
template <class Type>
const Type& NodeTree <Type>:: getElement() const
{
    return this->data;
}

/**
 * Getter de l'alçada del node
 * @return enter amb l'alçada del node
 */
template <class Type>
int NodeTree <Type>:: getHeight() const
{
    return this->height;
}



// -----------------------------------------
//    Funcions modificadores
// -----------------------------------------


/**
 * Setter de l'alçada del node
 * @param h enter corresponent a l'alçada del node
 */
template <class Type>
void NodeTree <Type>:: setHeight(int h)
{
    this->height = h;
    if(!this->isRoot())
    {
        //Si no és root, la seva alçada és el màxim entre la que ja tenia i la del fill que hem afegit.
        this->getParent()->setHeight(std::max(h+1, this->getParent()->getHeight()));
    }
}

/**
 * Setter de l'element que emmagatzema el node
 * @param data element que conté el node
 */
template <class Type>
void NodeTree <Type>::setData(const Type& data)
{
    this->data = data;
}

/**
 * Setter del fill esquerre
 * @param newLeft punter de NodeTree al nou fill esquerre
 */
template <class Type>
void NodeTree <Type>::setLeft(NodeTree<Type>* newLeft)
{
    this->pLeft = newLeft;
}

/**
 * Setter del fill dret
 * @param newRight punter de NodeTree al nou fill dret
 */
template <class Type>
void NodeTree <Type>::setRight(NodeTree<Type>* newRight)
{
    this->pRight = newRight;
}

/**
 * Setter del pare
 * @param newParent punter de NodeTree al nou pare
 */
template <class Type>
void NodeTree <Type>::setParent(NodeTree<Type>* newParent)
{
    this->pParent = newParent;
}