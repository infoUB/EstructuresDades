

#ifndef BINARYSEARCHTREE_HPP
#define BINARYSEARCHTREE_HPP

#include "NodeTree.hpp"
#include <iostream>
#include <stdexcept>

using namespace std;


template <class Type>
class BinarySearchTree
{
    public:
        /*Constructors i Destructors*/
        BinarySearchTree();
        BinarySearchTree(const BinarySearchTree& orig);
        virtual ~BinarySearchTree();
        
        /*Consultors*/
        int size() const;
        bool isEmpty() const;
        NodeTree<Type>* root();
        bool search(const Type& element);
        bool searchNode(const Type &element, const NodeTree<Type> &Node);
        void printInorder() const;
        void printPreorder() const;
        void printPostorder() const;
        int getHeight();
        
        /*Modificadors*/
        void insert(const Type& element); 
        BinarySearchTree<Type> *mirror();
        
    private:
        
        NodeTree<Type>* recursive_copy(NodeTree<Type> *orig_node);
        void postDelete(NodeTree<Type>* p);
        int size(NodeTree<Type>* p) const;
        void printPreorder(NodeTree<Type>* p) const;
        void printPostorder(NodeTree<Type>* p) const;
        void printInorder(NodeTree<Type>* p) const;
        int getHeight(NodeTree<Type>* p); 
        NodeTree<Type>* mirror_subtree(NodeTree<Type> *node);
        
        /*Atributs*/
        NodeTree<Type>* pRoot;
        int maxHeight;
};



// -----------------------------------------
//    Constructors i destructor
// -----------------------------------------

/**
 * Constructor per defecte de la classe BinarySearchTree
 */
template <class Type>
BinarySearchTree <Type>:: BinarySearchTree()
{
    this->pRoot = nullptr;
}


/**
 * Constructor còpia de la classe BinarySearchTree
 * @param orig Objecte de la classe BinarySearchTree, corresponenet a l'arbre
 *  que es vol copiar
 */
template <class Type>
BinarySearchTree<Type>::BinarySearchTree(const BinarySearchTree& orig)
{
    this->pRoot = new NodeTree<Type>(orig.pRoot->getElement()); //definim el node arrel amb el mateix element que l'arrel de l'arbre original
    if(orig.pRoot->hasLeft()) //Si té fill esquerre
    {
        NodeTree<Type> *l_node = recursive_copy(orig.pRoot->getLeft()); //Generem recursivament el subarbre amb arrel al node esquerre
        this->pRoot->setLeft(l_node); //Indiquem que és el fill dret de l'arrel
        l_node->setParent(this->pRoot); //Indiquem que l'arrel és el pare del node que hem generat
    }
    if(orig.pRoot->hasRight()) //Anàlogament amb el subarbre dret
    {
        NodeTree<Type> *r_node = recursive_copy(orig.pRoot->getRight());
        this->pRoot->setRight(r_node);
        r_node->setParent(this->pRoot);
    }  
}

/**
 * Destructor de la classe BinarySearchTree
 */
template <class Type>
BinarySearchTree<Type>::~BinarySearchTree()
{
    //Es podria fer servir la funció postDelete(), però tal com hem implementat
    //  el destructor de la classe NodeTree, no és necessari. Tan sols caldrà
    //  que cridem el destructor del node arrel i aquest anirà cridant 
    //  recursivament els destructors de tots els nodes de l'arbre.
    
    
    //postDelete(this->pRoot);
    delete this->pRoot; 
    this->pRoot = nullptr;
}



// -----------------------------------------
//    Funcions consultores
// -----------------------------------------


/**
 * Funció per obtenir el nombre total de nodes de l'arbre
 * @return entrer corresponent al nombre total de nodes
 */
template <class Type>
int BinarySearchTree <Type>:: size() const
{
    return size(this->pRoot);
}

/**
 * Funció per comprovar si l'arbre és buit o no
 * @return booleà indicant si està buit o no
 */
template <class Type>
bool BinarySearchTree <Type>:: isEmpty() const
{
    //En el nostre cas, hem definit que l'arbre està buit si el node arrel és nul
    return (this->pRoot == nullptr);
}

/**
 * Funció per obtenir el node arrel
 * @return punter a NodeTree de l'arrel de l'arbre
 */
template <class Type>
NodeTree<Type>* BinarySearchTree <Type>:: root()
{
    return this->pRoot;
}

/**
 * Fnució per comprovar si un element és a l'arbre
 * @param element referència a l'element que es vol cercar
 * @return boolèa indicant si l'element és a l'arbre o no
 */
template <class Type>
bool BinarySearchTree <Type>:: search(const Type &element)
{
    return searchNode(element, this->pRoot);
}

/**
 * Mètode auxiliar per buscar elements a l'arbre
 * @param element referència a l'element que es vol cercar
 * @param node referència al node arrel del subarbre on efectuar la cerca
 * @return booleà indicant si l'element és a l'arbre o no
 */
template <class Type>
bool BinarySearchTree <Type>:: searchNode(const Type &element, const NodeTree<Type> &node)
{
    //Si l'element del node eue ens han passat és el que busquem, retornem true
    if(node.getElement() == element)
    {
        return true;
    }
    
    //Si els subarbres on, segons les propietats del arbres de cerca binària s'hauria de trobar l'element, no existeixem
    if((element < node.getElement() && node.getLeft() == nullptr) || (element > node.getElement() && node.getRight() == nullptr))
    {
        return false;
    }
    
    //Si existeixen, agafem el node arrel del que ens interessa i cridem recursivament aquesta funció
    if(element > node.getElement())
    {
        return searchNode(element, node.getRight());
    }
    else
    {
        return searchNode(element, node.getLeft());
    }
}

/**
 * Mètode per imprimir en in-ordre el contingut de l'arbre
 */
template <class Type>
void BinarySearchTree <Type>:: printInorder() const
{
    if(this->isEmpty())
    {
        throw logic_error("L'arbre és buit!");
    }
    else
    {
        printInorder(this->pRoot);
    }
}

/**
 * Mètode per imprimir en pre-ordre el contingut de l'arbre
 */
template <class Type>
void BinarySearchTree <Type>:: printPreorder() const
{
    if(this->isEmpty())
    {
        throw logic_error("L'arbre és buit!");
    }
    else
    {
        printPreorder(this->pRoot);
    }
}

/**
 * Mètode per imprimir en post-ordre el contingut de l'arbre
 */
template <class Type>
void BinarySearchTree<Type>::printPostorder() const
{    
    if(this->isEmpty())
    {
        throw logic_error("L'arbre és buit!");
    }
    else
    {
        printPostorder(this->pRoot);
    }
}

/**
 * Mètode per obtenir l'alçada total de l'arbre
 * @return enter amb l'alçada
 */
template <class Type>
int BinarySearchTree <Type>:: getHeight()
{
    return this->pRoot->getHeight();
}


// -----------------------------------------
//    Funcions modificadores
// -----------------------------------------


/**
 * Mètode per afegir un element a l'arbre
 * @param element element que es vol afegir
 */
template <class Type>
void BinarySearchTree <Type>:: insert(const Type &element) 
{
    //Si l'arbre era buit
    if(this->pRoot == nullptr)
    {
        NodeTree<Type> *root = new NodeTree<Type>(element); //Creem el node arrel amb l'element
        root->setHeight(0); //Indiquem que l'alçada de l'arrel és 0
        this->maxHeight = 0; //Posem l'alçada màxima a 0;
        this->pRoot = root; //Indiquem que el node que hem creat és l'arrel de l'arbre
    }
    else
    {
        //Busquem l'element a l'arbre. Si hi és, llencem una axcepció, doncs no
        //  podem tenir dos nodes amb el mateix valor. En cas contrati, s'executarà
        //  la cerca i ens portarà fins a la fulla de la qual hauria de penjar el nou element.
        NodeTree<Type> *aux = this->pRoot;
        while(not ((element < aux->getElement() && aux->getLeft() == nullptr) || (element > aux->getElement() && aux->getRight() == nullptr)))
        {
            if(element == aux->getElement())
            {
                throw invalid_argument("L'element ja és a l'arbre!");
            }
            if(element < aux->getElement())
            {
                aux = aux->getLeft();
            }
            else
            {
                aux = aux->getRight();
            }
        }
        
        NodeTree<Type> *nou = new NodeTree<Type>(element); //Creem el nou node amb l'element 
        nou->setParent(aux); //El pare del nou és la fulla que hem trobat amb la cerca
        if(element > aux->getElement()) //Comprovem si nou ha de ser fill esquerre o dret
        {
            aux->setRight(nou);
        }
        else
        {
            aux->setLeft(nou);
        }
        nou->setHeight(0);
    }
}


/**
 * Mètode per fer l'arbre mirall
 * @return Retorna un punter de BinarySearchTree a un nou arbre mirall
 */
template<class Type>
BinarySearchTree<Type>* BinarySearchTree<Type>::mirror()
{
    //Mètode molt similar al constructor còpia, però canviant els punters als fills
    BinarySearchTree<Type>* mirror = new BinarySearchTree();
    mirror->pRoot = new NodeTree<Type>(this->pRoot->getElement()); //definim el node arrel amb el mateix element que l'arrel de l'arbre original
    if(this->pRoot->hasLeft()) //Si té fill esquerre
    {
        NodeTree<Type> *l_node = mirror_subtree(this->pRoot->getLeft()); //Generem recursivament el subarbre amb arrel al node esquerre
        mirror->root()->setRight(l_node); //Indiquem que és el fill dret de l'arrel
        l_node->setParent(mirror->pRoot); //Indiquem que l'arrel és el pare del node que hem generat
    }
    if(this->pRoot->hasRight()) //Anàlogament amb el subarbre dret
    {
        NodeTree<Type> *r_node = mirror_subtree(this->pRoot->getRight());
        mirror->root()->setLeft(r_node);
        r_node->setParent(this->pRoot);
    }  
    return mirror;
}



// -----------------------------------------
//    Funcions auxiliars
// -----------------------------------------


/**
 * Mètode auxiliar per fer la còpia d'un arbre en preordre
 * @param orig_node punter de NodeTree a arrel de l'arbre a copiar
 * @return Punter de NodeTree a l'arrel de l'arbre que hem copiat
 */
template<class Type>
NodeTree<Type>* BinarySearchTree<Type>::recursive_copy(NodeTree<Type> *orig_node)
{
    Type element = orig_node->getElement(); //Agafem l'element del node original
    NodeTree<Type> *new_node = new NodeTree<Type>(element); //Creem un nou node amb aquest element
    if(orig_node->hasLeft()) //Comprovem si té fill esquerre
    {
        NodeTree<Type> *l_node = recursive_copy(orig_node->getLeft()); //Fem la còpia recursiva a partir del fill esquerre del node original
        new_node->setLeft(l_node); //El fill esquerre del nou node és l'l_node que acabem de generar
        l_node->setParent(new_node); //El pare d'l_node és el node nou
    }
    if(orig_node->hasRight()) //Anàlogament per la dreta
    {
        NodeTree<Type> *r_node = recursive_copy(orig_node->getRight());
        new_node->setRight(r_node);
        r_node->setParent(new_node);
    }
    return new_node; //Retornem el node principal, ara amb tots els seus fills
}

/**
 * Funció auxiliar per eliminar l'arbre recursivament
 * @param p Punter de NodeTree a l'arrel de l'arbre que es vol eliminar
 */
template <class Type>
void BinarySearchTree<Type>::postDelete(NodeTree<Type> *p)
{
    if(p->hasLeft())
    {
        postDelete(p->getLeft());
    }
    if(p->hasRight())
    {
        postDelete(p->getRight());
    }
    delete p;
}

/**
 * Funció auxiliar per determinar el nombre de nodes d'un arbre
 * @param node punter de NodeTree a l'arrel de l'arbre que es vol mesurar
 * @return enter amb el nombre d'elements de l'arbre.
 */
template <class Type>
int BinarySearchTree <Type>:: size(NodeTree<Type> *node) const
{
    //El nombre de nodes d'un arbre és la suma del nombre de nodes dels subarbres amb arrel els fills de l'arrel de l'inicial
    int cont = 0; //Iniciem un contador local
    if(node->hasLeft())
    {
        cont += size(node->getLeft()); //Sumem els nodes del subarbre esquerra
    }
    if(node->hasRight())
    {
        cont += size(node->getRight()); //Sumem els nodes del subarbre dret
    }
    return cont + 1; //Sumem el node actual al total
}

/**
 * Mètode auxiliar per imprimir un arbre en pre-ordre
 * @param node punter de NodeTree al node arrel de l'arbre que es vol imprimir
 */
template <class Type> 
void BinarySearchTree<Type>::printPreorder(NodeTree<Type> *node) const
{
    std::cout << node->getElement() << " ";
    if(node->hasLeft())
    {
        printPreorder(node->getLeft());
    }
    if(node->hasRight())
    {
        printPreorder(node->getRight());
    }
}

/**
 * Mètode auxiliar per imprimir un arbre en post-ordre
 * @param node punter de NodeTree al node arrel de l'arbre que es vol imprimir
 */
template <class Type> 
void BinarySearchTree<Type>::printPostorder(NodeTree<Type> *node) const
{
    if(node->hasLeft())
    {
        printPostorder(node->getLeft());
    }
    if(node->hasRight())
    {
        printPostorder(node->getRight());
    }
    std::cout << node->getElement() << " ";
}

/**
 * Mètode auxiliar per imprimir un arbre en in-ordre
 * @param node punter de NodeTree al node arrel de l'arbre que es vol imprimir
 */
template <class Type> 
void BinarySearchTree<Type>::printInorder(NodeTree<Type> *node) const
{
    if(node->hasLeft())
    {
        printInorder(node->getLeft());
    }
    std::cout << node->getElement() << " ";
    if(node->hasRight())
    {
        printInorder(node->getRight());
    }
}

/**
 * Mètode auxiliar per obtenir l'alçada d'un node
 * @param node punter de NodeTree al node del qual es vol saber l'alçada
 */
template<class Type>
int BinarySearchTree<Type>::getHeight(NodeTree<Type>* node)
{
    return node->getHeight();
    
}

/**
 * Mètode auxiliar
 * @param node
 */
template<class Type>
NodeTree<Type>* BinarySearchTree<Type>::mirror_subtree(NodeTree<Type> *node)
{
    Type element = node->getElement(); //Agafem l'element del node original
    NodeTree<Type> *new_node = new NodeTree<Type>(element); //Creem un nou node amb aquest element
    if(node->hasLeft()) //Comprovem si té fill esquerre
    {
        NodeTree<Type> *l_node = mirror_subtree(node->getLeft()); //Fem la còpia recursiva a partir del fill esquerre del node original
        new_node->setRight(l_node); //Ara volem que el fill dret sigui el subarbre esquerre
        l_node->setParent(new_node); //El pare d'l_node és el node nou
    }
    if(node->hasRight()) //Anàlogament per la dreta
    {
        NodeTree<Type> *r_node = mirror_subtree(node->getRight());
        new_node->setLeft(r_node);
        r_node->setParent(new_node);
    }
    return new_node; //Retornem el node principal, ara amb tots els seus fills
}


#endif /* BINARYSEARCHTREE_HPP */

