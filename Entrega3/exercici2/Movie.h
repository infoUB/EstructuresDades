#ifndef MOVIE_H
#define MOVIE_H

#include <bits/stdc++.h>

using namespace std;

class Movie {
    public:
        /*Constructor i destructor*/
        Movie();
        Movie(int id, string title, float score);
        Movie(const Movie& orig);
        virtual ~Movie();

        /*Modificadors*/
        void set_id(int id);
        void set_title(string title);
        void set_score(float score);

        /*Consultors*/
        int get_id() const;
        string get_title() const;
        float get_score() const;
        string to_string() const;
        friend ostream& operator<<(ostream& os, const Movie& movie);
        Movie& operator=(const Movie& movie);

    private:
        int id;
        string title;
        float score;

};

#endif /* MOVIE_H */

