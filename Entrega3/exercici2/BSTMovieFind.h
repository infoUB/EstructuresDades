#ifndef BSTMOVIEFIND_H
#define BSTMOVIEFIND_H

#include "BinarySearchTree.hpp"
#include "Movie.h"
#include <bits/stdc++.h>

using namespace std; 

class BSTMovieFind {
    public:
        /*Constructors i destructor*/
        BSTMovieFind();
        BSTMovieFind(const BSTMovieFind& orig);
        virtual ~BSTMovieFind();

        /*Funcions consultores*/
        Movie findMovie(const int id) const;
        float findMovieRating(const int id) const;
        string showMovie(const int id) const;
        void printSorted() const;
        int getHeight() const;

        /*Funcions modificadores*/
        void appendMovies(string filename);
        void insertMovie(int movieID, string title, float rating);

    private:
        BinarySearchTree<Movie> *movies;

};

#endif /* BSTMOVIEFIND_H */

