#include "BSTMovieFind.h"

//-------------------------------------
//  Constructors i destructor
//-------------------------------------

/**
 * Constructor per defecte de BSTMovieFind
 */
BSTMovieFind::BSTMovieFind()
{
    this->movies = new BinarySearchTree<Movie>();
}

/**
 * Destructor
 */
BSTMovieFind::~BSTMovieFind() 
{
    delete movies;
}


//-------------------------------------
//  Funcions consultores
//-------------------------------------

/**
 * Funció per buscar una pel·lícula a l'arbre
 * @param id enter amb l'ID de la pel·lícula a buscar
 * @return punter a la pel·lícula si s'ha trobat, nullptr en cas contrari
 */
Movie BSTMovieFind::findMovie(const int id) const
{
    return this->movies->search(id);
}

/**
 * Mètode per aconseguir la puntuació d'una pel·lícula
 * @param id enter amb l'ID de la pel·lícula
 * @return float amb la puntuació de la pelicual
 */
float BSTMovieFind::findMovieRating(const int id) const
{
    return this->findMovie(id).get_score();    
}

/**
 * Mètode per obtenir tota l'informació d'una película
 * @param id enter amb l'ID de la pelicula
 * @return  string amb tota la informació
 */
string BSTMovieFind::showMovie(const int id) const
{
    return this->findMovie(id).to_string();
}

/**
 * Mètode per imprimir les pelicules per ordre creixent d'ID
 */
void BSTMovieFind::printSorted() const
{
    return this->movies->printSorted();
}

/**
 * Mètode per obtenir l'alçada de l'arbre
 * @return 
 */
int BSTMovieFind::getHeight() const
{
    return this->movies->getHeight();
}

//-------------------------------------
//  Funcions modificadores
//-------------------------------------

/**
 * Mètode per afegir pelicules mitjançant un fitxer
 * @param filename string amb el nom del fitxer.
 */
void BSTMovieFind::appendMovies(string filename)
{
    std::ifstream file(filename);
    string movie;
    while(getline(file, movie)) //Anem agafant línies
    {
        string id_string, title, rating_string; 
        int id;
        float rating;        
        int end = movie.find("::"); //Busquem el primer separador
        id_string = movie.substr(0, end); //Guardem la ID
        movie.erase(0, end + 2); //Esborrem la part que ja no volem llegir
        end = movie.find("::"); //Busquem el segon separador
        title = movie.substr(0, end); //Guardem el títol
        movie.erase(0, end+2); //Esborrem el títol i el separador
        rating_string = movie; //El que queda és la puntuació
        
        istringstream ss(id_string); //Passem la ID a enter
        ss >> id;
        istringstream ss2(rating_string); //Passem la puntuació a float
        ss2 >> rating;
        
        try
        {
            insertMovie(id, title, rating); //Inserim pel·lícula
        }
        catch(exception &e)
        {
            cout << e.what() << endl;
        }
    }
}

/**
 * Mètode per inserir una pel·lícula a l'arbre
 * @param movieID enter amb l'ID de la pel·lícula
 * @param title string amb el títol de la pel·lícula
 * @param rating float amb la puntuació de la pel·lícula
 */
void BSTMovieFind::insertMovie(int movieID, string title, float rating)
{
    try
    {
        Movie new_movie(movieID, title, rating);
        movies->insert(new_movie.get_id(), new_movie);
    }
    catch(exception &e)
    {
        throw e;
    }
}


