#include <bits/stdc++.h>
#include "circle.h"

//_________________________________________________________________________________________________
//CONSTRUCTORS AND DESTRUCTORD

circle::circle() //Default class constructor
{
    this->radius = 0;
}

circle::circle(float radius)
{
    this->radius = radius;
}

circle::circle(const circle& original)
{
    this->radius = original.radius;
}

circle::~circle()
{
}

//_________________________________________________________________________________________________
//GETTERS I SETTERS

float circle::get_radius()
{
    return this->radius;
}

void circle::set_radius(float radius)
{
    this->radius = radius;
}

//_________________________________________________________________________________________________
//ALTRES FUNCIONS

float circle::area()
{
    return 3.141592*this->radius*this->radius;
}



