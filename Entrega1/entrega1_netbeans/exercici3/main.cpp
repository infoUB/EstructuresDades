#include <bits/stdc++.h>
#include "circle.h"

using namespace std;


int  menu(const vector<string> &vec_options);
void create_circle(int & cont);


int main()
{
    
    vector<string> vec_options = {"1. Sortir", "2. Introduir cercle"};

    int opt;
    int cont = 1;
    
    while(1)
    {
        cout << "Hola, què vol fer?" << endl;
        opt = menu(vec_options);
    
        switch(opt)
        {
            case 1:
                return 0;
            case 2:
                try
                {
                    create_circle(cont);
                }
                catch(exception& e)
                {
                    cout << e.what() << endl;
                }
        }
    }
}



int  menu(const vector<string> &vec_options) //Li passem una referència amb const per estalviar memòria
{
    int opt;
    bool entrada_correcta;
    
    
    do{
        
        entrada_correcta = true; //Suposarem inicialment que l'entrada és correcta
        
        for(int i = 0; i < vec_options.size(); i++) //Mostrem opcions
        {
            cout << vec_options[i] << endl;
        }
        
        cin >> opt; //Agafem entrada de l'usuari
                
        if(cin.fail()) //Si l'entrada no és del tipus desitjat
        {
            entrada_correcta = false; //L'entrada no és correcta
            cin.clear(); //Esborrem el buffer de cin
            cin.ignore(); //Diem que ignori l'entrada
            cout << "Opció no vàlida. Introduiexi un enter." << endl << endl;
        }
        else if(opt <= 0 || opt > vec_options.size()) //Si és un enter però fora de rang
        {
            entrada_correcta = false; //L'entrada no és vàlida
            cout << "Opció fora de rang!" << endl;
        }
        
        cout << endl;
       
    }
    while(!entrada_correcta);
    
    return opt;
}



void create_circle(int &n)
{

    float radius;

    cout << "Cercle número " << n << ": Introdueixi el radi del cercle:" << endl;
    cin >> radius;

    if(radius <= 0)
    {
        throw invalid_argument("Valor no vàlid. El radi ha de ser estrictament positiu");
    }
    else
    {
        circle cercle(radius);
        cout << "L'àrea d'aqest cercle és de " << cercle.area() << endl;
        n++;
    }
    cout << endl;
}



