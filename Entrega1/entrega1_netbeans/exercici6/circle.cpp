#include <bits/stdc++.h>
#include "circle.h"

//_________________________________________________________________________________________________
//CONSTRUCTORS AND DESTRUCTORD

circle::circle():ellipse() //Default class constructor
{
    cout << "Constructor per defecte cercle" << endl;
}

circle::circle(float radius):ellipse(radius, radius)
{
    cout << "Constructor parametritzat cercle" << endl;
}

circle::~circle()
{
    cout << "Destructor cercle" << endl;
}


//_________________________________________________________________________________________________
//GETTERS I SETTERS

float circle::get_radius()
{
    return radius1;
}

void circle::set_radius(float radius)
{
    this->ellipse::set_radius(radius, radius);
}

//_________________________________________________________________________________________________
//ALTRES FUNCIONS

float circle::area()
{
    return this->radius1*this->radius1*3.141592;
}



