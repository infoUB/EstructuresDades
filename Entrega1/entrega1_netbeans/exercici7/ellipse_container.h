#ifndef ELLIPSE_CONTAINER_H
#define ELLIPSE_CONTAINER_H

#include <bits/stdc++.h>
#include "ellipse.h"

using namespace std;

class ellipse_container 
{
    private:
        vector<ellipse*> container;
    
    public:
        ellipse_container();
        ellipse_container(const ellipse_container& orig);
        ~ellipse_container();
        
        void add_ellipse(ellipse* elipse);
        float get_areas();
};

#endif //ELLIPSE_CONTAINER_H

