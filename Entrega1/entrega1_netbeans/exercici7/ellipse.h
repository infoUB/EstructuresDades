#ifndef ELLIPSE_H
#define ELLIPSE_H

#include <bits/stdc++.h>

using namespace std;

class ellipse
{
    protected:
        float radius1;
        float radius2;

    public:
        
        //Class constructors and destructors
        ellipse();
        ellipse(float radius1, float radius2);
        ellipse(const ellipse& original);
        virtual ~ellipse();

        //Getters and setters
        float get_radius1();
        float get_radius2();
        void set_radius1(float radius1);
        void set_radius2(float radius2);
        void set_radius(float radius1, float radius2);

        //Other methods
        virtual float area();
};

#endif //ELIPSE_H



