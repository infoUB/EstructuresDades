#include <bits/stdc++.h>
#include "circle.h"
#include "ellipse.h"
#include "ellipse_container.h"

using namespace std;

int  menu(const vector<string> &vec_options);
void create_circle(int & cont_circle, std::istream& in, ellipse_container& container);
void create_ellipse(int & cont_ellipse, std::istream& in, ellipse_container& container);



int main()
{

    //Variable auxiliar per l'entrada i contadors
    int opt;
    int cont_ellipse = 0; 
    int cont_circle = 0;
    
    ellipse_container *container = new ellipse_container(); //Creem el contenidor d'elipses dinàmicament
    
    vector<string> vec_options = {"1. Sortir", "2. Afegir figura", "3. Glossari de figures", "4. Introduir figures des de fitxer", "5. Calcula la suma de les àrees"};

    while(1) //Mentre no es surti del programa (no es faci un return)
    {

        char fig_type; //On quardarem el tipus de figuta
        string in_file; //Nom del possible fitxer dentrada
        
        cout << "Hola, què vol fer?" << endl;
        
        opt = menu(vec_options); //Imprimim menú i llegim entrada.
    
        switch(opt) //Switch sobre les diverses opcions
        {
            case 1: //Sortir
                delete container; //Eliminem el ellipse_container creat dinàmicament
                return 0;

            case 2: //Afegir figura
                cout << "Introduiexi les dades de la figura (tipus[C o E] data1 data2[buit si el tipus és C])" << endl;
                cin >> fig_type; //Llegim tipus de figura
                if(fig_type == 'C') //Si és cercle
                {
                    try
                    {
                        create_circle(cont_circle, std::cin, *container); //Intentem crear cercle. Si l'entrada no és vàlida, llença excepció.
                    }
                    catch(exception& e) //Capturem l'excepció
                    {
                        cout << e.what() << endl; //Mostrem el missatge de l'excepció
                    }
                }
                else if(fig_type == 'E') //Si és el·lipse
                {
                    try
                    {
                        create_ellipse(cont_ellipse, std::cin, *container); //Demanem mesures a l'usuari i intentem crear l'el·lipse
                    }
                    catch(exception& e) //Capturem l'excepció
                    {
                        cout << e.what() << endl; //Mostrem el missatge de l'excepció
                    }
                }
                else //Si l'entrada no és ni C ni E
                {
                    cout << "Figura no definida, disculpin les molèsties." << endl;
                }
                break;

            case 3: //Mostrar els contadors de figures.
                cout << "Hi ha " << cont_circle << " cercles i " << cont_ellipse << " ellipses." << endl;
                break; 
            
            case 4: //Creear figures a paritr d'un fitxer. Noteu que en aquest case hi ha "{}", ja que són necessaris si hi declarem variables a dins. 
            {
                cout << "Introdueixi el nom del fitxer a llegir:" << endl;
                cin >> in_file; //Introduim el nom del fitxer
                ifstream file(in_file); //Definim un nou input stream pel fitxer

                while(!file.eof()) //Mentre no s'arribi al final del fitxer
                {
                    file >> fig_type; //Llegim tipus de figura
                    if(fig_type == 'C') //Si és cercle
                    {
                        try
                        {
                            create_circle(cont_circle, file, *container); //Intentem crear cercle. Si l'entrada no és vàlida, llença excepció.
                        }
                        catch(exception& e)
                        {
                            cout << e.what() << endl;
                        }
                    }
                    else if(fig_type == 'E') //Si és el·lipse
                    {
                        try
                        {
                            create_ellipse(cont_ellipse, file, *container); //Demanem mesures a l'usuari i intentem crear l'el·lipse
                        }
                        catch(exception& e)
                        {
                            cout << e.what() << endl;
                        }
                    }
                    else //Si l'entrada no és ni C ni E
                    {
                        cout << "Figura no definida, disculpin les molèsties." << endl;
                    }
                }
                break;
            }
            
            case 5:
                cout << "La suma de les àrees és de " << container->get_areas() << endl; //Fem servir -> perquè m de dereferenciar el punter a ellipse_container
                break;

        }
    }
}


//Mètode per mostrar el menú. Retorna l'entrada de l'usuari, que assegurem que és vàlida
int  menu(const vector<string> &vec_options) //Li passem una referència amb const per estalviar memòria
{
    int opt;
    bool entrada_correcta;
    
    do{
        
        entrada_correcta = true; //Suposarem inicialment que l'entrada és correcta
        
        for(int i = 0; i < vec_options.size(); i++) //Mostrem opcions
        {
            cout << vec_options[i] << endl;
        }
        
        cin >> opt; //Agafem entrada de l'usuari
                
        if(cin.fail()) //Si l'entrada no és del tipus desitjat
        {
            entrada_correcta = false; //L'entrada no és correcta
            cin.clear(); //Esborrem el buffer de cin
            cin.ignore(); //Diem que ignori l'entrada
            cout << "Opció no vàlida. Introduiexi un enter." << endl << endl;
        }
        else if(opt <= 0 || opt > vec_options.size()) //Si és un enter però fora de rang
        {
            entrada_correcta = false; //L'entrada no és vàlida
            cout << "Opció fora de rang!" << endl;
        }
        
        cout << endl;
       
    }
    while(!entrada_correcta);
    
    return opt;
}


//Mètode per crear una el·lipse. Agafa com a paràmetre una referència al contador de cercles, el input stream desitjat i el contenidor on s'afegirà
void create_circle(int &n, std::istream& in, ellipse_container& container)
{

    float radius;

    //cout << "Cercle número " << n+1 << ": Introdueixi el radi del cercle:" << endl;
    in >> radius;

    if(radius <= 0)
    {
        throw invalid_argument("Valor no vàlid. El radi ha de ser estrictament positiu");
    }
    else
    {
        ellipse *cercle_obj = new circle(radius); //Encara que volem un cercle, com que cercle hereda de elipse, podem declarar-lo de tipus elipse i instanciar un cercle.
        container.add_ellipse(cercle_obj); //Afegim el cercle al container
        cout << "L'àrea d'aqest cercle és de " << cercle_obj->area() << endl;
        n++;
        //delete cercle_obj;        
    }
    
    cout << endl;
}




//Mètode per crear una el·lipse. Agafa com a paràmetre una referència al contador de cercles i el input stream desitjat
void create_ellipse(int &n, std::istream& in, ellipse_container& container)
{

    float radius1;
    float radius2;

    //cout << "El·lipse número " << n+1 << ": Introdueixi els radis de l'el·lipse:" << endl;
    in >> radius1 >> radius2;

    if(radius1 <= 0 && radius2 <= 0) //Comprova la validesa dels radis
    {
        throw invalid_argument("Valor no vàlid. El radi ha de ser estrictament positiu");
    }
    else
    {
        ellipse *elipse_obj = new ellipse(radius1, radius2); //generem l'objecte
        container.add_ellipse(elipse_obj); //Afegim l'el·lipse al container
        cout << "L'àrea d'aqest el·lipse és de " << elipse_obj->area() << endl; //Imprimim la esva àrea
        n++; //Incrementem el contador d'el·lipses (un cop creada l'el·lipse)
        //delete elipse_obj;
    }
    cout << endl;
    
}


/**
 * RESPOSTES A LES PREGUNTES:
 * 1.- Ens permet definir una classe general amb comportaments i dades comuns i classes derivades més específiques
 *      que hereden aquests comportaments i dades i en poden afegir de més específics.
 * 2.- Suposem el següent codi:
 *      ellipse* cercle = new cercle(radi);
 *      cout << cercle.area();
 * 
 *      Si area() de ellipse no fos virtual, al cridar area sobre cercle es cridaria el mètode de la clase ellipse
 *      i no el de la calsse cercle, que és el que volem que cridi. En aquest cas els dos mètodes són molt similars,
 *      però en un cas general poden ser molt diferents.   
 * 
 * 3.- Perquè les classes derivades són més específiques. Sabem que un cercle ha heredat d'el·lipse, per tant necessitem 
 *      construir el cercle i aquest construirà la el·lipse, i el mateix en el cas del destructor: destruïm la classe filla i
 *      aquesta destrueix la classe mare. En canvi al revés no ho podem fer. Coneixem la calsse mare però no sabem quina és
 *      la classe filla. En aquest cas només cercle heresda d'el·lipse, però en un cas més general no sabem de quin tipus és
 *      la classe que volem que heredi de la mare.
 * 
 * 4.- No, ha de ser un mètode d'objecte ja que necessitem els atributs propis dels objectes (els radis).
 * 
 * 5.- Tenim:
 *      - Radi de circle: private ja que no l'hem d'utilitzar fora de la classe
 *      - Radis de ellipse: protected ja que circle els haurà de modificar
 *      - Vector de ellipse_container: que és privat ja que no necessitem tenir-hi accés des de fora. Ja tenim mètodes que fan les
 *              diverses operacions necessàries.
 * 
 * 6.- Incrementa en 1, ja que tan sols volem anar a la posició següent.
 
 */

