#include <bits/stdc++.h>
#include "circle.h"
#include "ellipse.h"

using namespace std;

int  menu(const vector<string> &vec_options);
void create_circle(int & cont_circle);
void create_ellipse(int & cont_ellipse);



int main()
{

    //Variable auxiliar per l'entrada i contadors
    int opt;
    int cont_ellipse = 0; 
    int cont_circle = 0;
    
    vector<string> vec_options = {"1. Sortir", "2. Afegir figura", "3. Glossari de figures"};

    while(1) //Mentre no es surti del programa (no es faci un return)
    {

        char fig_type; //On quardarem el tipus de figuta
        cout << "Hola, què vol fer?" << endl;
        
        opt = menu(vec_options); //Imprimim menú i llegim entrada.
    
        switch(opt) //Switch sobre les diverses opcions
        {
            case 1: //Sortir
                return 0;

            case 2: //Afegir figura
                cout << "Introduiexi el tipus de figura [C o E]" << endl;
                cin >> fig_type; //Llegim tipus de figura
                if(fig_type == 'C') //Si és cercle
                {
                    try
                    {
                        create_circle(cont_circle); //Intentem crear cercle. Si l'entrada no és vàlida, llença excepció.
                    }
                    catch(exception& e)
                    {
                        cout << e.what() << endl;
                    }
                }
                else if(fig_type == 'E') //Si és el·lipse
                {
                    try
                    {
                        create_ellipse(cont_ellipse); //Demanem mesures a l'usuari i intentem crear l'el·lipse
                    }
                    catch(exception& e)
                    {
                        cout << e.what() << endl;
                    }
                }
                else //Si l'entrada no és ni C ni E
                {
                    cout << "Figura no definida, disculpin les molèsties." << endl;
                }
                break;

            case 3: //Mostrar els contadors de figures.
                cout << "Hi ha " << cont_circle << " cercles i " << cont_ellipse << " ellipses." << endl;
                break; 


        }
    }
}


//Mètode per mostrar el menú. Retorna l'entrada de l'usuari, que assegurem que és vàlida
int  menu(const vector<string> &vec_options) //Li passem una referència amb const per estalviar memòria
{
    int opt;
    bool entrada_correcta;
    
    do{
        
        entrada_correcta = true; //Suposarem inicialment que l'entrada és correcta
        
        for(int i = 0; i < vec_options.size(); i++) //Mostrem opcions
        {
            cout << vec_options[i] << endl;
        }
        
        cin >> opt; //Agafem entrada de l'usuari
                
        if(cin.fail()) //Si l'entrada no és del tipus desitjat
        {
            entrada_correcta = false; //L'entrada no és correcta
            cin.clear(); //Esborrem el buffer de cin
            cin.ignore(); //Diem que ignori l'entrada
            cout << "Opció no vàlida. Introduiexi un enter." << endl << endl;
        }
        else if(opt <= 0 || opt > vec_options.size()) //Si és un enter però fora de rang
        {
            entrada_correcta = false; //L'entrada no és vàlida
            cout << "Opció fora de rang!" << endl;
        }
        
        cout << endl;
       
    }
    while(!entrada_correcta);
    
    return opt;
}


//Mètode per crear un cercle. Agafa com a paràmetre una referència al contador de cercles.
void create_circle(int &n)
{

    float radius;

    cout << "Cercle número " << n+1 << ": Introdueixi el radi del cercle:" << endl;
    cin >> radius;

    if(radius <= 0)
    {
        throw invalid_argument("Valor no vàlid. El radi ha de ser estrictament positiu");
    }
    else
    {
        circle cercle_obj(radius);
        cout << "L'àrea d'aqest cercle és de " << cercle_obj.area() << endl;
        n++;
    }
    cout << endl;
}




//Mètode per crear una el·lipse. Agafa com a paràmetre una referència al contador de cercles
void create_ellipse(int &n)
{

    float radius1;
    float radius2;

    cout << "El·lipse número " << n+1 << ": Introdueixi els radis de l'el·lipse:" << endl;
    cin >> radius1 >> radius2;

    if(radius1 <= 0 && radius2 <= 0) //Comprova la validesa dels radis
    {
        throw invalid_argument("Valor no vàlid. El radi ha de ser estrictament positiu");
    }
    else
    {
        ellipse elipse_obj(radius1, radius2); //generem l'objecte
        cout << "L'àrea d'aqest ellipse és de " << elipse_obj.area() << endl; //Imprimim la esva àrea
        n++; //Incrementem el contador d'el·lipses (un cop creada l'el·lipse)
    }
    cout << endl;
}

