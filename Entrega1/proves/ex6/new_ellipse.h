#ifndef ELLIPSE_H
#define ELLIPSE_H

#include <bits/stdc++.h>

using namespace std;

class ellipse
{
    protected:
        float radius1;
        float radius2;

    public:
        
        //Class constructors and destructors
        ellipse()
        {
            this->radius1 = 0;
            this->radius2 = 0;
            cout << "Constructor per defecte ellipse" << endl;
        }
        ellipse(float radius1, float radius2)
        {
            this->radius1 = radius1;
            this->radius2 = radius2;
            cout << "Constructor paràmetres ellipse" << endl;
        }
        ellipse(ellipse& original)
        {
            this->radius1 = original.get_radius1();
            this->radius2 = original.get_radius2();
            cout << "Constructor còpia ellipse" << endl;
        }            
        ~ellipse(){}

        //Getters and setters
        float get_radi()
        {
            return this->radius1;
        }
        float get_radius1()
        {
            return this->radius1;
        }
        float get_radius2()
        {
            return this->radius2;
        }
        void set_radius1(float radius1)
        {
            this->radius1 = radius1;
        }
        void set_radius2(float radius2)
        {
            this->radius2 = radius2;
        }
        void set_radius(float radius1, float radius2)
        {
            this->radius1 = radius1;
            this->radius2 = radius2;
        }


        //Other methods
        virtual float area()
        {
            return this->radius1*this->radius2*3.141592;
        }
};

class circle : public ellipse
{
    public:
    circle():ellipse()
    {
        cout << "Constructor per defecte cercle" << endl;
    }
    circle(float radius):ellipse(radius, radius)
    {
        cout << "Constructor paràmetres cercle" << endl;
    }
    int get_radius()
    {
        return radius1;
    }
    void set_radius(float radius)
    {
        this->ellipse::set_radius(radius, radius);
    }
    float area()
    {
        return this->radius1*this->radius1*3.141592;
    }
};

#endif //ELIPSE_H
