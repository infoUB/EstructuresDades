#include <bits/stdc++.h>
#include "circle.h"

//_________________________________________________________________________________________________
//CONSTRUCTORS AND DESTRUCTORD

circle::circle() //Default class constructor
{
	cout << "Constructor de circle" << endl;
    this->set_radius1(0);
    this->set_radius2(0);
}

circle::circle(float radius)
{
	cout << "Constructor de circle" << endl;

    this->set_radius1(radius);
    this->set_radius2(radius);
}

circle::circle(circle& original)
{
	cout << "Constructor de circle" << endl;
    this->set_radius1(original.get_radius1());
    this->set_radius2(original.get_radius1());
}

circle::~circle()
{
}


//_________________________________________________________________________________________________

float circle::get_radius()
{
    return this->get_radius1();
}

void circle::set_radius(float radius)
{
    this->set_radius1(radius);
}

//_________________________________________________________________________________________________


float circle::area()
{
    return 3.141592*this->get_radius1()*this->get_radius1();
}



