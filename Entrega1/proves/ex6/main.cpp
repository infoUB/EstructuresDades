#include <bits/stdc++.h>
//#include "circle.h"
#include "new_ellipse.h"

using namespace std;

int  menu();
void create_circle(int & cont_circle, std::istream& in);
void create_ellipse(int & cont_ellipse, std::istream& in);

int main()
{

    //Variable auxiliar per l'entrada i contadors
    int opt;
    int cont_ellipse = 0; 
    int cont_circle = 0;
    


    while(1) //Mentre no es surti del programa (no es faci un return)
    {

        char fig_type; //On quardarem el tipus de figuta
        string in_file; //Nom del fitxer a obrir
        cout << "Hola, què vols fer?" << endl;
        
        opt = menu(); //Imprimim menú i llegim entrada.
    
        switch(opt) //Switch sobre les diverse opcions
        {
            case 1: //Sortir
                return 0;

            case 2: //Afegir figura
                cout << "Introduiexi les dades de la figura (tipus[C o E] data1 data2[buit si el tipus és c])" << endl;
                cin >> fig_type; //Llegim tipus de figura
                if(fig_type == 'C') //Si és cercle
                {
                    try
                    {
                        create_circle(cont_circle, std::cin); //Intentem crear cercle. Si l'entrada no és vàlida, llença excepció.
                    }
                    catch(int e)
                    {
                        cout << "S'ha produït una excepció" << endl;
                    }
                }
                else if(fig_type == 'E') //Si és el·lipse
                {
                    try
                    {
                        create_ellipse(cont_ellipse, std::cin); //Demanem mesures a l'usuari i intentem crear l'el·lipse
                    }
                    catch(int e)
                    {
                        cout << "S'ha produït una excepció" << endl;
                    }
                }
                else //Si l'entrada no és ni C ni E
                {
                    cout << "Figura encara no definida, disculpin les molèsties." << endl;
                }
                break;

            case 3: //Mostrar els contadors de figures.
                cout << "Hi ha " << cont_circle << " cercles i " << cont_ellipse << " ellipses." << endl;
                break; 

            case 4:
                cout << "Introdueixi el nom del fitxer a llegir:" << endl;
                cin >> in_file;
                ifstream file(in_file);

                while(!file.eof())
                {
                    file >> fig_type; 
                    if(fig_type == 'C') //Si és cercle
                    {
                        try
                        {
                            create_circle(cont_circle, file); //Intentem crear cercle. Si l'entrada no és vàlida, llença excepció.
                        }
                        catch(int e)
                        {
                            cout << "S'ha produït una excepció" << endl;
                        }
                    }
                    else if(fig_type == 'E') //Si és el·lipse
                    {
                        try
                        {
                            create_ellipse(cont_ellipse, file); //Demanem mesures a l'usuari i intentem crear l'el·lipse
                        }
                        catch(int e)
                        {
                            cout << "S'ha produït una excepció" << endl;
                        }
                    }
                    else //Si l'entrada no és ni C ni E
                    {
                        cout << "Figura encara no definida, disculpin les molèsties." << endl;
                    }
                }
                break;


        }
    }
}


//Mètode per mostrar el menú. Retorna l'entrada de l'usuari, que assegurem que és vàlida
int  menu()
{
    int opt;

    vector<string> vec_options = {"1. Sortir", "2. Afegir figura", "3. Glossari de figures", "4. Introduir figures des de fitxer"};
    do{
        //Recorrem el vector d'opcions
        for(int i = 0; i < vec_options.size(); i++)
        {
            cout << vec_options[i] << endl;
        }
        cin >> opt; //Llegim opció de l'usuari
    }
    while(opt <= 0 || opt > vec_options.size()); //Comprivem si és vàlida

    return opt; //retornem la opció vàlida.
}


//Mètode per crear un cercle. Agafa com a paràmetre una referència al contador de cercles i el input stream desitjat
void create_circle(int &n, std::istream& in)
{

    float radius;

    in >> radius;

    if(radius <= 0) //Llemcem excepció si radi no vàlid
    {
        cout << "Atenció: valor no vàlid. El radi ha de ser estrictament positiu" << endl;
        throw 0;
    }
    else
    {
        circle cercle(radius); //Creem el cercle
        cout << "L'àrea d'aqest cercle és de " << cercle.area() << endl; //Imprimim la seva àrea
        n++; //Incrementem en 1 el contador de cercles.
    }
}


//Mètode per crear una el·lipse. Agafa com a paràmetre una referència al contador de cercles i el input stream desitjat
void create_ellipse(int &n, std::istream& in)
{

    float radius1;
    float radius2;

    in >> radius1 >> radius2;

    if(radius1 <= 0 && radius2 <= 0) //Comprova la validesa dels radis
    {
        cout << "Atenció: valor no vàlid. El radi ha de ser estrictament positiu" << endl;
        throw 0;
    }
    else
    {
        ellipse ellipse(radius1, radius2); //generem l'objecte
        cout << "L'àrea d'aqest ellipse és de " << ellipse.area() << endl; //Imprimim la esva àrea
        n++; //Incrementem el contador d'el·lipses.
    }
}

