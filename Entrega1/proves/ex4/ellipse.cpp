#include <bits/stdc++.h>
#include "ellipse.h"

//_________________________________________________________________________________________________
//CONSTRUCTORS AND DESTRUCTORD

ellipse::ellipse() //Default class constructor
{
	cout << "Constructor d'ellipse" << endl;
    this->radius1 = 0;
    this->radius2 = 0;
}

ellipse::ellipse(float radius1, float radius2)
{
	cout << "Constructor d'ellipse" << endl;
    this->radius1 = radius1;
    this->radius2 = radius2;
}

ellipse::ellipse(ellipse& original)
{
	cout << "Constructor d'ellipse" << endl;
    this->radius1 = original.radius1;
    this->radius2 = original.radius2;
}

ellipse::~ellipse()
{
}


//_________________________________________________________________________________________________
//GETTERS AND SETTERS

float ellipse::get_radius1()
{
    return this->radius1;
}

float ellipse::get_radius2()
{
    return this->radius2;
}

void ellipse::set_radius1(float radius1)
{
    this->radius1 = radius1;
}

void ellipse::set_radius2(float radius2)
{
    this->radius2 = radius2;
}

//_________________________________________________________________________________________________
//OTHER METHODS

float ellipse::area()
{
    return 3.141592*this->radius1*this->radius2;
}



