#ifndef CIRCLE_H
#define CIRCLE_H

#include <bits/stdc++.h>

using namespace std;

class circle
{
    private:
        float radius;

    public:
        
        //Class constructors and destructors
        circle();
        circle(float radius);
        circle(circle& original);
        ~circle();

        //Getters and setters
        float get_radius();
        void set_radius(float radius);

        //Other methods
        float area();
};

#endif //CIRCLE_H
