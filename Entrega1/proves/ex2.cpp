#include <bits/stdc++.h>

using namespace std;

int  menu();

int main()
{


    string nom;
    string arr_options[] = {"1. Sortir", "2. Benvinguda", "3. Redefinir nom"};
    vector<string> vec_options;

    for(int i = 0; i < 3; i++)
    {
        vec_options.push_back(arr_options[i]);
    }

    
    int opt;

    cout << "Hola, com et dius?" << endl;
    cin >> nom;
    while(1)
    {
        cout << "Hola, " << nom << ". Què vols fer?" << endl;
        opt = menu();
    
        switch(opt)
        {
            case 1:
                return 0;
            case 2:
                cout << "Benvingut/da a l'assignatura d'estructura de dades, " << nom << " ." << endl;
                break;
            case 3:
                cout << "Introduiex el nom nou: " << endl;
                cin >> nom;
                break;
        }
    }
}

int  menu()
{
    int opt;

    vector<string> vec_options = {"1. Sortir", "2. Benvinguda", "3. Redefinir nom"};
    do{

        for(int i = 0; i < vec_options.size(); i++)
        {
            cout << vec_options[i] << endl;
        }
        cin >> opt;
    }
    while(opt <= 0 || opt > vec_options.size());

    return opt;
}
