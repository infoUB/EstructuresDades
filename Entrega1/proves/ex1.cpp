#include <bits/stdc++.h>

using namespace std;

int main()
{


    string nom;
    string arr_options[] = {"1. Sortir", "2. Benvinguda"};
    int opt;

    cout << "Hola, com et dius?" << endl;
    cin >> nom;

    do
    {
        cout << "Hola, " << nom << ", què vols fer?" << endl;

        for(int i = 0; i < 2; i++)
        {
            cout << arr_options[i] << endl;
        }

        cin >> opt;

        if(opt == 2)
        {
            cout << "Benvingut/da a l'assignatura d'estructura de dades " << nom << endl;
        }
    }
    while(opt != 1);
}
