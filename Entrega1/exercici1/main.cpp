/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: agarcise78.alumnes
 *
 * Created on 19 / de febrer / 2018, 12:53
 */

#include <iostream>
#include <string>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    string nom;
    string arr_options[] = {"Sortir", "Benvinguda"};
    int opt;
    
    cout << "Hola, com et dius? " << endl; 
    cin >> nom;
    do
    {
        cout << "Hola, " << nom << ", què vols fer?" << endl;

        for(int i = 0; i < 2; i++)
        {
            cout << i+1 << ". " << arr_options[i] << endl;
        }

      
        cin >> opt;
        if(opt == 2)
        {
            cout << "Benvingut/da a l'assignatura d'estructura de dades " << nom << endl; 
        }
        
        
    }
    while(opt != 1);
    
    return 0;
    
    
}

