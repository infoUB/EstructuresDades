/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: mherralo19.alumnes
 *
 * Created on 19 / de febrer / 2018, 12:54
 */

#include <cstdlib>
#include <iostream>

using namespace std;

int main(int argc, char** argv) {
    string nom;
    int opc;
    string menu [] = {"1. Sortir", "2. Benvinguda"};
    
    cout << "Hola, com et dius?" << endl;
    cin >> nom;
    
    do{
        cout << "Hola, " << nom << ", què vols fer?" << endl;
        for (int i=0; i < 2; i++){
            cout << menu[i] << endl;
        }
        cin >> opc;
        
        if (opc == 2){
            cout << "Benvingut/da a l'assignatura d'estructura de dades " << nom << "." << endl;
        }
    }
    while (opc != 1);
}

