/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: agarcise78.alumnes
 *
 * Created on 19 / de febrer / 2018, 13:32
 */

#include <iostream>
#include <string>
#include <vector> 
using namespace std;

/*
 * 
 */
int main()
{
    string nom;
    string arr_options[] = {"Sortir", "Benvinguda", "Redefinir nom"};
    vector<string> vec_options;
    
    for(int i = 0; i < 3; i++)
    {
        vec_options.push_back(arr_options[i]);
    }
    
    int opt;
    
    cout << "Hola, com et dius? " << endl; 
    cin >> nom;
    do
    {
        cout << "Hola, " << nom << ", què vols fer?" << endl;

        for(int i = 0; i < vec_options.size(); i++)
        {
            cout << i+1 << ". " << vec_options[i] << endl;
        }

      
        cin >> opt;
        if(opt == 2)
        {
            cout << "Benvingut/da a l'assignatura d'estructura de dades " << nom << endl; 
        }
        else if(opt == 3)
        {
            cout << "Introdueix el nou nom: ";
            cin >> nom;
        }
        
        
    }
    while(opt != 1);
    
    return 0;   
    
}